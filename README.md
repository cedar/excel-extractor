# Instruction to run the code

Install pip: https://pip.pypa.io/en/stable/installing/

Install virtualenv: https://virtualenv.pypa.io/en/stable/installation/

Activate virtualenv:

`virtualenv .env`

`source .env/bin/activate`

Install required dependencies:

`pip install -r requirements.txt`

# Important commands:

Prepare input for the extractor
`sh prepare_input.sh`

Extract RDF files from all available Excel files in 'all_excel_files.csv'

`python main.py --run`

Extract RDF files from **N** random Excel files in 'all_excel_files.csv'

`python main.py --run multiple N --dev`


Extract RDF file from an Excel file

`python extractor.py <path of your Excel file> (single|multiple)`

# Tests

All tests can be executed using the following command

`pytest`

For testing only the Extractor

`sh test_extractor.sh`

# Evaluation
The dataset for evaluation is folder `evaluation/`

The evaluation results are stored in `evaluation_per_table.csv`

# Excel and HTML tables
They are stored `/data/tcao/insee_data/` on CEDAR cluster
