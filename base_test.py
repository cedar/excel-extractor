from extractor import ExcelExtractor

def extract_file(file_name):
    extractor = ExcelExtractor()
    list_list_tables = extractor.extract_file(file_name, sheet_idx=[0])
    if list_list_tables:
        tables = list_list_tables[0]
        if tables:
            table = tables[0]
            header_rows = table.header_zone_x.headers
            header_columns = table.header_zone_y.headers
            data_rows = table.data_zone.data_rows
        else:
            header_rows, header_columns, data_rows = [], [], []
    else:
        header_rows, header_columns, data_rows = [], [], []

    return header_rows, header_columns, data_rows

def template_test(file_name, expected_header_rows, expected_header_columns, expected_data_rows):
    header_rows, header_columns, data_rows = extract_file(file_name)
    assert data_rows == expected_data_rows
    assert header_rows == map(list, zip(*expected_header_rows))
    assert header_columns == map(list, zip(*expected_header_columns))
    assert len(data_rows) == len(header_columns)

def template_test_header_rows(file_name, expected_header_rows, expected_header_columns):
    header_rows, header_columns, data_rows = extract_file(file_name)
    assert header_rows == expected_header_rows
    assert header_columns == expected_header_columns

def extract_multiple_tables(file_name, list_expected_headers):
    extractor = ExcelExtractor()
    extracted_results = extractor.extract_file(file_name, sheet_idx=[0])[0]
    assert len(extracted_results) == len(list_expected_headers)

    for index, table in enumerate(extracted_results):
        header_rows = table.header_zone_x.headers
        header_columns = table.header_zone_y.headers
        assert header_rows == list_expected_headers[index][0]
        assert header_columns == list_expected_headers[index][1]
