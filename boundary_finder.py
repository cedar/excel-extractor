import copy

import xlrd
from xlrd.sheet import Sheet

import utils
from config import *
from data_model import EmptyZone, ExtractableZone, RowSignature, TableMetadata
from data_provider import DataProvider
from excel_utils import ExcelValidator
from log_utils import logger

TABLE_REGEX = 't*h+t*(D+t*)+h*'
"""
Regular expression to check if several rows could form a valid table
"""
DATA_CELLS_REGEX = 't*D+t*'
"""
Regular expression to check if several rows are data cells with some text comments (optional) at the bottom
"""
MIN_TABLE_HEIGHT = 3
"""
The mininum number of rows in a table
"""
MIN_TABLE_WIDTH = 2
"""
The mininum number of columns in a table
"""

class BoundaryFinder():
    """
    Finding the boundary between tables or groups of tables
    """

    def __init__(self, sheet, data_provider):
        """
        Args:
            sheet (`xlrd.sheet`)
            data_provider (`DataProvider`)
        """
        self.sheet = sheet
        self.data_provider = data_provider
        # a dictionary which maps each row's id to a list of all cell's data type from that row
        self.dict_rows_signatures = dict()
        # last row id of `sheet`
        self.last_row_id = self.data_provider.get_row_count() - 1
        # we'll identify an `ExtractableZone` where we can extract tables
        # the left column of `ExtractableZone`
        self.left_border = 0
        # the right column of table in `ExtractableZone` which has smallest value
        self.left_most_border = None
        # the right column of table in `ExtractableZone` which has biggest value
        self.right_most_border = None
        # the list of cells that we should consider as empty after extracting tables
        self.empty_zones_post_extraction = list()
        # the list of cells that we should consider as empty before extracting tables
        self.empty_zones_pre_extraction = list()

    def __build_row_signatures(self, extractable_zone):
        """
        Args:
            extractable_zone (`ExtractableZone`)

        Returns:
            a dictionary which maps each row's id to a list of all cell's data type from that row
        """
        dict_rows_signatures = dict()
        # also build a list of empty rows in order to identify the horizontal boundaries between tables
        self.empty_row_ids = list()
        self.empty_row_ids.append(extractable_zone.top)
        for rx in xrange(extractable_zone.top, extractable_zone.bottom + 1):
            row = self.data_provider.get_row(rx, extractable_zone)
            types = list()
            for col_id, _ in enumerate(row):
                ctype = self.__get_cell_type(rx, col_id + extractable_zone.left)
                types.append(ctype)

            dict_rows_signatures[rx] = ''.join(types)

            if ExcelValidator.is_empty(row):
                self.empty_row_ids.append(rx)

        return dict_rows_signatures

    def __build_list_separations(self):
        """
        Returns:
            a list of row's id (`int`) from horizontal lines that separate
            tables
        """
        list_separations = list()
        if len(self.empty_row_ids) > 1:
            groups = utils.group_nearby_values(self.empty_row_ids, max_distance=3)
            list_separations = [group[0] for group in groups]
            if self.last_row_id not in list_separations and self.last_row_id > max(list_separations):
                list_separations += [self.last_row_id]
        logger.debug('\tSeparations of tables are: ' + str(list_separations))
        return list_separations

    def __build_list_table_bounding_box(self, list_separations, extractable_zone):
        """
        Returns:
            a list of tuples (previous_separation, separation, first_column_id, last_column_id)
                 from an `ExtractableZone`, each tuple could be used to identify a `Table`
        """
        logger.debug("\tBuild list of table's bounding box for " + str(extractable_zone))
        list_table_bounding_box = list()
        for index, separation in enumerate(list_separations):
            if index > 0:
                previous_separation = list_separations[index - 1]
                row_dominant_type_vectors = list()
                range_of_rows = range(previous_separation, separation)
                first_column_id = self.__find_first_column_id(extractable_zone, range_of_rows)
                last_column_id = self.__find_last_column_id(extractable_zone, range_of_rows)

                first = first_column_id - extractable_zone.left
                last = last_column_id - first_column_id
                for row_id in range_of_rows:
                    row_signature = self.dict_rows_signatures[row_id][first:last]
                    row_type = self.__classify_row_signature(row_signature)
                    if row_type:
                        row_dominant_type_vectors.append(row_type)

                rows_types_expression = str(''.join(row_dominant_type_vectors))
                logger.debug('\t\ttop = %d, bottom = %d, rows_types_expression = %s' % (
                    previous_separation,
                    separation,
                    rows_types_expression
                ))
                if utils.match_exact(TABLE_REGEX, rows_types_expression):
                    logger.debug('\t\tfound a bounding box')
                    list_table_bounding_box.append((previous_separation, separation, first_column_id, last_column_id))
                # this bounding box contains data rows (and some optional title rows on the top)
                # it should be merged with the previous bounding box (the header rows and some data rows
                # of the same table)
                elif utils.match_exact(DATA_CELLS_REGEX, rows_types_expression):
                    if len(list_table_bounding_box) > 0:
                        logger.debug('\t\tmerge this bounding box into the previous one')
                        top, _, first_col, last_col = list_table_bounding_box[-1]
                        list_table_bounding_box[-1] = (top, separation, first_col, last_col)
                else:
                    logger.debug('\t\tno bounding box found')
        return list_table_bounding_box

    def __identify_extractable_zones(self):
        """
        Returns:
            a list of `ExtractableZone` starting from `self.left_border`, these zones
            are separated by horizontal empty row(s). Returns `None` if no zone
            could be found
        """
        logger.debug('Identify list of ExtractableZone from left border ' + str(self.left_border))

        default_right_column = self.left_border + MIN_TABLE_WIDTH

        def __search_box(top, set_right_column=True):
            left_column = self.left_border
            top_row_id = top
            bottom_row_id = top_row_id + MIN_TABLE_HEIGHT
            row_id = bottom_row_id
            if set_right_column:
                return left_column, default_right_column, bottom_row_id, top_row_id, row_id
            else:
                return left_column, None, bottom_row_id, top_row_id, row_id

        if default_right_column >= self.data_provider.get_col_count():
            return None
        else:
            extractable_zones = list()
            left_column, right_column, bottom_row_id, top_row_id, row_id = __search_box(0)

            # an empty column on the right and an empty row on the bottom could
            # identify a potential region to extract a table
            while right_column < self.data_provider.get_col_count():
                if extractable_zones:
                    top_row_id = extractable_zones[-1].bottom
                else:
                    top_row_id = 0
                left_column, _, bottom_row_id, top_row_id, row_id = __search_box(top_row_id, set_right_column=False)

                while row_id < self.data_provider.get_row_count():
                    column = self.data_provider.get_col(right_column)[top_row_id:row_id + 1]
                    bottom_row = self.data_provider.get_row(row_id, left_index=left_column, right_index=right_column + 1)

                    right_border = None
                    if ExcelValidator.is_empty(column):
                        if not self.data_provider.column_contains_merged_cells(right_column, top_row_id, row_id + 1):
                            right_border = right_column - 1
                    elif right_column == self.data_provider.get_col_count() - 1:
                        right_border = right_column

                    if right_border:
                        if not self.data_provider.row_contains_merge_cells(row_id, left_column, right_column + 1) and \
                            (row_id == self.data_provider.get_row_count() - 1 or ExcelValidator.is_empty(bottom_row)):
                            extractable_zones.append(
                                ExtractableZone(left_column, right_border, top_row_id, row_id)
                            )
                            logger.debug('\tfound extractable_zone ' + str(extractable_zones[-1]))
                            logger.debug('\t\trow_id: %s, left_column %s, right_column %s, top_row_id %s' % (
                                str(row_id), str(left_column), str(right_column), str(top_row_id)
                            ))
                            logger.debug('\t\tempty column ' + str(ExcelValidator.is_empty(column)))
                            logger.debug('\t\tlast column ' + str(right_column == self.data_provider.get_col_count() - 1))
                            left_column, right_column, bottom_row_id, top_row_id, row_id = __search_box(row_id + 1)
                            continue
                    row_id += 1
                right_column += 1
            return extractable_zones

    def __identify_extractable_region(self):
        """
        Returns:
            a tuple (`extractable_zone`, `list_empty_zones_post_extraction`, `list_empty_zones_pre_extraction`)
                `extractable_zone`: a rectangle spans from the first row to the last row of spreadsheet
                    starting from `self.left_border`. We can extract tables from this rectangle
                'list_empty_zones_post_extraction': a list of `EmptyZone` to restrict some cells from
                    these zones as empty after we finish extracting tables
                'list_empty_zones_pre_extraction': a list of `EmptyZone` to restrict some cells from
                    these zones as empty before we continue extracting tables starting from `self.left_border`
        """
        extractable_zones = self.__identify_extractable_zones()
        if extractable_zones is None:
            return None, list(), list()

        left_column = self.left_border

        # before extraction: all cells from right border to the right should be considered as empty
        list_empty_zones_pre_extraction = list()
        # after extraction: all cells from left-most border to the right should be considered as empty
        list_empty_zones_post_extraction = list()
        if extractable_zones and len(extractable_zones) > 1:
            list_borders = [extractable_zone.right for extractable_zone in extractable_zones]
            self.right_most_border = max(list_borders)
            self.left_most_border = min(list_borders)
            logger.debug('\tlist of borders is ' + str(list_borders))
            logger.debug('\tleft-most border is ' + str(self.left_most_border))
            logger.debug('\tright-most border is ' + str(self.right_most_border))
            for extractable_zone in extractable_zones:
                logger.debug('\tcheck ' + str(extractable_zone))

                signatures = self.__build_row_signatures(extractable_zone)

                row_dominant_type_vectors = list()
                for row_id in xrange(extractable_zone.top, extractable_zone.bottom + 1):
                    row_signature = signatures[row_id]
                    row_type = self.__classify_row_signature(row_signature)
                    if row_type:
                        row_dominant_type_vectors.append(row_type)

                rows_types_expression = str(''.join(row_dominant_type_vectors))
                logger.debug('\t\trows_types_expression = %s' % rows_types_expression)
                if 'D' not in rows_types_expression:
                    logger.debug('\t\tinvalid ExtractableZone')
                    continue

                if extractable_zone.right + 1 < self.right_most_border:
                    list_empty_zones_pre_extraction.append(EmptyZone(
                        extractable_zone.right + 1, self.right_most_border,
                        extractable_zone.top, extractable_zone.bottom
                    ))
                if extractable_zone.right > self.left_most_border:
                    list_empty_zones_post_extraction.append(EmptyZone(
                        self.left_most_border + 1, extractable_zone.right,
                        extractable_zone.top, extractable_zone.bottom
                    ))

            if not self.empty_zones_post_extraction:
                self.left_border = self.right_most_border + 1
            extractable_zone = ExtractableZone(left_column, self.right_most_border, 0, self.data_provider.get_row_count() - 1)
        else:
            logger.debug('Found no ExtractableZones')
            extractable_zone = ExtractableZone(left_column, self.data_provider.get_col_count() - 1, 0, self.data_provider.get_row_count() - 1)
            self.left_border = self.data_provider.get_col_count()

        logger.debug('Identify a region to extract table(s)')
        logger.debug('\t' + str(extractable_zone))
        logger.debug('\tlist_empty_zones_post_extraction: ' + utils.str_list(list_empty_zones_post_extraction))
        logger.debug('\tlist_empty_zones_pre_extraction: ' + utils.str_list(list_empty_zones_pre_extraction))
        return extractable_zone, list_empty_zones_post_extraction, list_empty_zones_pre_extraction

    def post_extract(self, list_table_metadata):
        """
        Adjust the left border and assign some cells as empty after extracting tables
        from `ExtractableZone`
        """
        if self.empty_zones_post_extraction:
            logger.debug('\tpost extract')
            self.left_border = self.left_most_border + 1
            self.data_provider.set_empty_zones(self.empty_zones_post_extraction)
        if list_table_metadata and len(list_table_metadata) == 1 and self.right_most_border:
            self.left_border = self.right_most_border + 1

    def contains_tables(self):
        """
        Returns:
            True: if we can continue extract tables from the given `sheet`
        """
        if self.right_most_border and self.right_most_border == self.data_provider.get_col_count() - 1:
            return False
        return True

    def next_list_table_metadata(self, mode):
        """
        Args:
            mode (string): "multiple" to identify multiple tables per sheet,
                "single" to identify only 1 table per sheet
        Returns:
            list of `TableMetadata`
        """
        if mode not in ['single', 'multiple']:
            raise ValueError('mode must be either "single" or "multiple"')

        if mode == 'single':
            extractable_zone = ExtractableZone(0, self.data_provider.get_col_count() - 1,
                                               0, self.data_provider.get_row_count() - 1)
        elif mode == 'multiple':
            extractable_zone, self.empty_zones_post_extraction, self.empty_zones_pre_extraction = self.__identify_extractable_region()

            if self.empty_zones_pre_extraction:
                logger.debug('\tpre extract')
                if self.contains_tables():
                    self.data_provider.set_empty_zones(self.empty_zones_pre_extraction, clear_old_empty_cells=False)
                else:
                    logger.debug('\tall tables found, skip assigning empty cells')

        logger.debug('Find list of TableMetadata from ' + str(extractable_zone))
        if not extractable_zone:
            logger.debug('\tThere is no TableMetadata')
            return None
        else:
            list_table_metadata = list()
            self.dict_rows_signatures = self.__build_row_signatures(extractable_zone)

            if mode == 'multiple':
                list_separations = self.__build_list_separations()
                if list_separations:
                    list_table_bounding_box = self.__build_list_table_bounding_box(list_separations, extractable_zone)
                    for (previous_separation, separation, first_column_id, last_column_id) in list_table_bounding_box:
                        range_of_rows = range(previous_separation, separation + 1)
                        list_table_metadata.append(TableMetadata(
                            first_column_id,
                            self.__identify_first_column_of_data_cells(range_of_rows, extractable_zone.left, extractable_zone.right),
                            last_column_id,
                            range_of_rows,
                            copy.deepcopy(self.dict_rows_signatures)
                        ))

            if not list_table_metadata:
                range_of_rows = xrange(self.data_provider.get_row_count())
                first_column_id = self.__find_first_column_id(extractable_zone, range_of_rows)
                last_column_id = self.__find_last_column_id(extractable_zone, range_of_rows)
                list_table_metadata.append(TableMetadata(
                    first_column_id,
                    self.__identify_first_column_of_data_cells(range_of_rows, extractable_zone.left, extractable_zone.right),
                    last_column_id,
                    range_of_rows,
                    copy.deepcopy(self.dict_rows_signatures)
                ))

            logger.debug('\tFound list of TableMetadata: ' + utils.str_list(list_table_metadata))
            return list_table_metadata

    def __identify_first_column_of_data_cells(self, range_of_rows, first_column_id, last_column_id):
        """
        Returns:
            the first column id (`int`) of data cells
        """
        list_row_signatures = list()
        for row_id in range_of_rows:
            if row_id in self.dict_rows_signatures:
                list_row_signatures.append(self.dict_rows_signatures[row_id])

        # scan table from right to left for finding the first column of data zone
        for index, column_id in enumerate(reversed(xrange(first_column_id, last_column_id + 1))):
            index = last_column_id - first_column_id - index
            column_signature = ''.join([row_signature[index] for row_signature in list_row_signatures])
            if self.__is_empty_column(column_signature):
                continue
            elif self.__is_header_column(column_signature):
                if column_id < last_column_id - 1:
                    return column_id + 1
        return None

    def __find_first_column_id(self, extractable_zone, range_of_rows=None):
        """
        Returns:
            the first column id (`int`) of a given `ExtractableZone` and a `range` of rows
        """
        first_column_id = extractable_zone.left
        while first_column_id <= extractable_zone.right:
            if not range_of_rows:
                column = self.data_provider.get_col(first_column_id)
            else:
                column = self.data_provider.get_col(first_column_id)[range_of_rows[0]: range_of_rows[-1]]

            if ExcelValidator.is_empty(column):
                first_column_id += 1
            else:
                break
        return first_column_id

    def __find_last_column_id(self, extractable_zone, range_of_rows=None):
        """
        Returns:
            the last column id (`int`) of a given `ExtractableZone` and a `range` of rows
        """
        last_column_id = extractable_zone.right
        while last_column_id >= extractable_zone.left:
            if not range_of_rows:
                column = self.data_provider.get_col(last_column_id)
            else:
                column = self.data_provider.get_col(last_column_id)[range_of_rows[0]: range_of_rows[-1]]

            if ExcelValidator.is_empty(column):
                last_column_id -= 1
            else:
                return last_column_id + 1
        return last_column_id

    def __is_header_column(self, column_signature):
        return utils.match_exact('(0|1)*1+0*', column_signature)

    def __is_empty_column(self, column_signature):
        return utils.match_exact('0+', column_signature)

    def __classify_row_signature(self, row_signature):
        """
        Returns:
            type of a row given its signature from ``__build_row_signatures``, 3 possible
            values are RowSignature.ROW_TYPE_DATA, RowSignature.ROW_TYPE_HEADER, RowSignature.ROW_TYPE_TITLE
        """
        if row_signature:
            if utils.match_exact('1+[2]+', row_signature):
                return RowSignature.ROW_TYPE_DATA
            elif utils.match_exact('[0]*[1]+', row_signature) or utils.match_exact('0+[2]+', row_signature):
                return RowSignature.ROW_TYPE_HEADER
            elif utils.match_exact('0*[1]+([0|1])*', row_signature):
                return RowSignature.ROW_TYPE_TITLE
            else:
                if float(sum((1 for c in row_signature if c == '2'))) / len(row_signature) >= 0.5:
                    return RowSignature.ROW_TYPE_DATA
        return None

    def __get_cell_type(self, rol_id, col_id):
        """
        Returns:
            type of a cell given its ``row_id`` and ``col_id``,
            if this cell belongs to merged cell, returns the type of that merged cell,
            possible values are xlrd.XL_CELL_EMPTY, xlrd.XL_CELL_NUMBER, xlrd.XL_CELL_TEXT
        """
        cell = self.data_provider.get_merged_cell(rol_id, col_id)
        ctype = cell.ctype
        if ExcelValidator.is_cell_empty_or_blank(cell):
            ctype = xlrd.XL_CELL_EMPTY
        elif ExcelValidator.contains_missing_value(cell):
            ctype = xlrd.XL_CELL_NUMBER
        else:
            if self.data_provider.belongs_to_merged_cell(rol_id, col_id):
                ctype = xlrd.XL_CELL_TEXT
            elif col_id == 0:
                ctype = xlrd.XL_CELL_TEXT
            elif ExcelValidator.contains_numeric_value(cell):
                ctype = xlrd.XL_CELL_NUMBER

        return str(ctype)
