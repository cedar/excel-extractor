import os
from log_utils import logger
from extractor import all_excel_files_map

V1_VS_V2_RESULT = 'v1_vs_v2.csv'
V2_ERRORS = 'v2_errors.txt'

def compare(excel_file_path):
    extracted_directory = excel_file_path.replace('.xls', '')
    file_name = os.path.basename(excel_file_path).replace('.xls', '')

    directory_1 = extracted_directory.replace(file_name, 'single_' + file_name)
    if not os.path.exists(directory_1):
        return None

    directories_1 = os.listdir(directory_1)

    num_matched = 0
    num_mismatched = 0
    num_multiple_tables = 0
    num_v2_errors = 0
    for directory in directories_1:
        sheet_directory = os.path.join(directory_1, directory)
        rdf_file = '0.ttl'
        rdf_file_path_1 = os.path.join(sheet_directory, rdf_file)
        if not os.path.exists(rdf_file_path_1):
            continue

        rdf_file_path_2 = rdf_file_path_1.replace('single_', 'multiple_')
        if os.path.exists(rdf_file_path_2):
            sheet_directory_2 = sheet_directory.replace('single_', 'multiple_')
            num_tables_2 = len(os.listdir(sheet_directory_2))

            if num_tables_2 > 1:
                num_multiple_tables += 1
            else:
                content_1 = open(rdf_file_path_1).read()
                content_2 = open(rdf_file_path_2).read()
                if content_1 == content_2:
                    num_matched += 1
                else:
                    num_mismatched += 1
        else:
            num_v2_errors += 1
    return num_matched, num_mismatched, num_multiple_tables, num_v2_errors

def compare_all_files():
    with open(V1_VS_V2_RESULT, 'w+') as csv_file:
        csv_file.write('num_matched, num_mismatched, num_multiple_tables, num_v2_errors, file_path\n')
        files = all_excel_files_map.keys()

        counter = 0
        total = len(files)
        for _file in files:
            results = compare(_file)
            if results:
                num_matched, num_mismatched, num_multiple_tables, num_v2_errors = results
                csv_file.write('%d, %d, %d, %d, %s\n' % (
                    num_matched, num_mismatched, num_multiple_tables, num_v2_errors, _file
                ))
                counter += 1
                if counter % 100 == 0:
                    print 'Progress', counter, total
        print 'DONE'

def list_v2_errors():
    set_v2_error_files = set()
    with open(V1_VS_V2_RESULT) as csv_file:
        lines = [line.replace('\n', '') for line in csv_file.readlines()][1:]
        for line in lines:
            items = line.split(', ')
            num_v2_errors = int(items[-2])
            if num_v2_errors > 0:
                set_v2_error_files.add(items[-1])

    with open(V2_ERRORS, 'w+') as f:
        for _file in set_v2_error_files:
            f.write('%s\n' % _file)

def summarize_comparing_result():
    with open(V1_VS_V2_RESULT) as f:
        lines = [line.replace('\n', '') for line in f.readlines()][1:]
        total_num_matched, total_num_mismatched, total_num_multiple_tables, total_num_v2_errors = \
            0, 0, 0, 0
        for line in lines:
            num_matched, num_mismatched, num_multiple_tables, num_v2_errors, _ = line.split(', ')
            total_num_matched += int(num_matched.strip())
            total_num_mismatched += int(num_mismatched.strip())
            total_num_multiple_tables += int(num_multiple_tables.strip())
            total_num_v2_errors += int(num_v2_errors)

        # 69473 2634 1683 273
        print 'total_num_matched', total_num_matched
        print 'total_num_mismatched', total_num_mismatched
        print 'total_num_multiple_tables', total_num_multiple_tables
        print 'total_num_v2_errors', total_num_v2_errors

if __name__ == '__main__':
    """
    Compare the extracted RDF files of `ExcelExtractor` mode `"single"` and mode `"multiple"`
    """
    compare_all_files()
    list_v2_errors()
    summarize_comparing_result()
