"""
All the data models used by this project
"""

class ExtractableZone:
    """
    A rectangle region of Excel sheet where we can extract `Table` instance
    """

    def __init__(self, left, right, top, bottom):
        """
        Args:
            left, right, top, bottom (`int`)
        """
        self.left = left
        self.right = right
        self.top = top
        self.bottom = bottom

    def __str__(self):
        return 'ExtractableZone: {left=%d, right=%d, top=%d, bottom=%d}' % (
            self.left, self.right, self.top, self.bottom
        )

class EmptyZone(ExtractableZone):
    """
    A rectangle region of Excel sheet where all cells should be considered as empty cells
    """

    @classmethod
    def belongs_to_empy_zones(cls, list_empty_zones, row_id, col_id):
        for empty_zone in list_empty_zones:
            if empty_zone.left <= col_id and col_id <= empty_zone.right and \
                empty_zone.top <= row_id and row_id <= empty_zone.bottom:
                return True
        return False

    def __str__(self):
        return 'EmptyZone: {left=%d, right=%d, top=%d, bottom=%d}' % (
            self.left, self.right, self.top, self.bottom
        )

class TableMetadata():
    """
    Relevant information to extract a `Table` instance
    """

    def __init__(self, first_column, first_column_of_data_cells, last_column, range_of_rows, dict_rows_signatures):
        """
        Args:
            first_column (`int`): the left-most column
            first_column_of_data_cells (`int` or `None`): the first column of data zone
            last_column (`int`): the right-most column
            range_of_rows (range `int`): top and bottom rows of table
            dict_rows_signatures (`dict` {`int`:`str`}): mapping between row id and its signature,
                check `BoundarFinder.__build_row_signatures` for more details
        """
        self.first_column = first_column
        self.first_column_of_data_cells = first_column_of_data_cells
        self.last_column = last_column
        self.range_of_rows = range_of_rows
        self.dict_rows_signatures = dict_rows_signatures

    def __str__(self):
        return "TableMedata {first_column: %s, first_column_of_data_cells: %s, last_column: %s, range_of_rows: [%s->%s]}" % (
            str(self.first_column), str(self.first_column_of_data_cells),
            str(self.last_column),
            str(self.range_of_rows[0]) if len(self.range_of_rows) > 0 else 'None',
            str(self.range_of_rows[-1]) if len(self.range_of_rows) > 0 else 'None'
        )

class HeaderZone():
    """
    Representing the header zone (header rows/columns)
    """

    def __init__(self, headers, axis):
        """
        A collection of header cells of a `Table` instance
            headers (list of list of string): each element of headers represents a header row (x axis) / column (y axis)
            axis (string): 'x' for x axis, 'y' for y axis
        """
        self.headers = headers
        self.axis = axis

class DataZone():
    """
    Representing the data zone of a `Table` instance
    """

    def __init__(self, data_rows, data_row_idx, first_row=None, last_row=None):
        """
        A collection of data cells of a Table object
            data_rows (list of list of `str`): each element of data_rows represents a data row
                (list of cells in the same row)
            data_row_idx (list of `int`): ids of all rows in data_rows
            first_row (`int`): id of the first row in this DataZone
            last_row (`int`): id of the last row in this DataZone
        """
        self.data_rows = data_rows
        self.data_row_idx = data_row_idx
        self.first_row = first_row
        self.last_row = last_row

class Table():
    """
    Representing a table extracted from Excel sheet
    """

    def __init__(self, header_zone_x=HeaderZone([], 'x'), header_zone_y=HeaderZone([], 'y'),
        data_zone=DataZone([], []), title='', comment=''):
        """
        A table which consists of headers and data cells
            title (string):
            comment (string):
            header_zone_x, header_zone_y (HeaderZone):
            data_zone (DataZone):
        """
        self.title = title
        self.comment = comment
        self.header_zone_x = header_zone_x
        self.header_zone_y = header_zone_y
        self.data_zone = data_zone

class VariableSheet():
    """
    Representing the "List of variables" sheet, we can use the information from this sheet
    to decode the encoded header cells from other sheet
    """

    def __init__(self, list_variables, code_name_definions):
        """
        Args:
            list_variables
            code_name_definions
        """
        self.list_variables = list_variables
        self.code_name_definions = code_name_definions

    def has_encoded_header(self):
        return self.list_variables and self.code_name_definions

class RowSignature:
    ROW_TYPE_TITLE = 't'
    ROW_TYPE_HEADER = 'h'
    ROW_TYPE_DATA = 'D'
