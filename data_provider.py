import xlrd
from xlrd.sheet import Cell

import utils
from config import *
from data_model import EmptyZone
from excel_utils import MISSING_VALUE_NOTATIONS, ExcelValidator
from log_utils import logger


class DataProvider():
    """
    Providing cells, rows, columns from `Sheet`
    """

    def __init__(self, sheet):
        """
        Args:
            sheet (`xlrd.sheet`)
        """
        self.sheet = sheet

        self.dict_merged_cells = self.__build_dict_merged_cells()
        self.dict_empty_cells = dict()

        self.row_count = None
        self.col_count = None

        # the cache of columns/rows in order to access them faster
        self.dict_cols = dict()
        self.dict_rows = dict()

    @classmethod
    def data_cell_value(cls, cell):
        """
        Returns:
            value of a given data cell (`xlrd.sheet.Cell`), possible returned data types
            are `float`, `str` and `None`
        """
        if ExcelValidator.is_cell_empty_or_blank(cell) or cell.ctype == xlrd.XL_CELL_BLANK \
            or ExcelValidator.contains_missing_value(cell):
            return None

        try:
            float(cell.value)
        except ValueError:
            return str(cell.value.encode('utf-8'))

        return float(cell.value)

    @classmethod
    def cell_value(cls, cell):
        """
        Returns:
            string value of a given cell (`xlrd.sheet.Cell`)
        """
        if cell.ctype == xlrd.XL_CELL_NUMBER:
            string = str(cell.value)
            if string.endswith('.0'):
                string = string[:-2]
            return string
        else:
            return utils.clean_empty_spaces(cell.value.strip())

    def __belongs_to_empy_zones(self, row_id, col_id):
        """
        Returns:
            True: if the cell at row `row_id` and column `col_id` is an empty cell
        """
        return len(self.dict_empty_cells) > 0 and (row_id, col_id) in self.dict_empty_cells

    def __empty_cell(self, row_id, col_id):
        return self.dict_empty_cells[(row_id, col_id)]

    def clear_empty_cells(self):
        """
        Some cells were masked as empty ones in order to identify the correct table's boundaries.
        To continue the extraction process for the other group of tables, we need to unmask these cells.
        """
        logger.debug('\t\tclear empty cells')
        self.dict_empty_cells.clear()
        # also need to clear the cache of rows and columns otherwise they will
        # contain the out-of-date cell's values
        self.dict_cols.clear()
        self.dict_rows.clear()

    def set_empty_zones(self, list_empty_zones, clear_old_empty_cells=True):
        """
        Consider some cells from ``list_empty_zones`` as empty

        Args:
            list_empty_zones (list `EmptyZone`)
        """
        if clear_old_empty_cells:
            self.clear_empty_cells()
        for empty_zone in list_empty_zones:
            logger.debug('\t\tset_empty_zones ' + str(empty_zone))
            for col_id in xrange(empty_zone.left, empty_zone.right + 1):
                for row_id in xrange(empty_zone.top, empty_zone.bottom + 1):
                    if col_id in self.dict_cols:
                        del self.dict_cols[col_id]
                    if row_id in self.dict_rows:
                        del self.dict_rows[row_id]
                    self.dict_empty_cells[(row_id, col_id)] = \
                        Cell(xlrd.XL_CELL_EMPTY, '', self.sheet.cell(row_id, col_id).xf_index)

    def get_row(self, row_id, extractable_zone=None, left_index=None, right_index=None):
        """
        Args:
            row_id (int): the id of row
            extractable_zone (ExtractableZone): the zone to which this row belongs
            left_index (int): starting position of this row
            right_index (int): ending position of this row
        Returns:
            collection of cells of a row
        """
        if row_id not in self.dict_rows:
            self.dict_rows[row_id] = [self.cell(row_id, col_id) for col_id in xrange(self.get_col_count())]
        if left_index and right_index:
            return self.dict_rows[row_id][left_index:min(self.get_col_count(), right_index + 1)]
        elif extractable_zone:
            return self.dict_rows[row_id][extractable_zone.left:extractable_zone.right + 1]
        else:
            return self.dict_rows[row_id]

    def get_col(self, col_id):
        """
        Returns:
            a column given its column id ``col_id``
        """
        if col_id not in self.dict_cols:
            self.dict_cols[col_id] = [self.cell(row_id, col_id) for row_id in xrange(self.get_row_count())]
        return self.dict_cols[col_id]

    def get_row_count(self):
        """
        Returns:
            number of rows omitting the empty rows at the bottom of `Sheet`
        """
        if self.row_count is None:
            self.row_count = self.sheet.nrows
            num_continuous_empty_rows = 0
            for rx in range(self.sheet.nrows):
                if ExcelValidator.is_empty(self.sheet.row(rx)):
                    num_continuous_empty_rows += 1
                    if num_continuous_empty_rows >= MAX_NUM_OF_EMPTY_ROWS:
                        self.row_count = rx - MAX_NUM_OF_EMPTY_ROWS
                        break
                else:
                    num_continuous_empty_rows = 0
        return self.row_count

    def get_col_count(self):
        """
        Returns:
            number of columns omitting the empty columns at the right of `Sheet`
        """
        if self.col_count is None:
            right_most_empty_col_id = self.sheet.ncols - 1
            for col_id in reversed(xrange(self.sheet.ncols)):
                if ExcelValidator.is_empty(self.sheet.col(col_id)):
                    right_most_empty_col_id = col_id
                else:
                    break
            self.col_count = right_most_empty_col_id + 1

        return self.col_count

    def cell(self, row_id, col_id):
        """
        Returns:
            a cell from given ``row_id`` and ``col_id``
        """
        if self.__belongs_to_empy_zones(row_id, col_id):
            return self.__empty_cell(row_id, col_id)
        return self.sheet.cell(row_id, col_id)

    def belongs_to_merged_cell(self, row_id, col_id):
        """
        Returns:
            True if the cell from given ``row_id`` and ``col_id`` belongs to a merged cell
            otherwise returns False
        """
        if self.__belongs_to_empy_zones(row_id, col_id):
            return False

        for crange, cell in self.dict_merged_cells.iteritems():
            row_low, row_high, column_low, column_high = crange
            if row_low <= row_id and row_id < row_high and column_low <= col_id and col_id < column_high:
                return True
        return False

    def column_contains_merged_cells(self, column_id, top_row, bottom_row):
        """
        Returns:
            True if the given column contains merged cells
        """
        for row_id in xrange(top_row, bottom_row + 1):
            if self.belongs_to_merged_cell(row_id, column_id):
                return True
        return False

    def row_contains_merge_cells(self, row_id, left_column, right_column):
        """
        Returns:
            True if the given row contains merged cells
        """
        for column_id in xrange(left_column, right_column + 1):
            if self.belongs_to_merged_cell(row_id, column_id):
                return True
        return False

    def get_merged_cell(self, row_id, col_id):
        """
        Returns:
            a merged cell at row `row_id` and column `col_id`
        """
        if self.__belongs_to_empy_zones(row_id, col_id):
            return self.__empty_cell(row_id, col_id)

        for crange, cell in self.dict_merged_cells.iteritems():
            row_low, row_high, column_low, column_high = crange
            if row_low <= row_id and row_id < row_high and column_low <= col_id and col_id < column_high:
                return cell
        return self.sheet.cell(row_id, col_id)

    def __build_dict_merged_cells(self):
        """
        Return:
            a dictionary that map a boundary of merged cell (bottom row, top row, left column, right column) into
            the combination of all cells (`xlrd.Sheet.Cell`) in that boundary
        """
        dict_merged_cells = dict()
        for crange in self.sheet.merged_cells:
            rlo, rhi, clo, chi = crange
            text_value = ''
            xf_index = None
            for rowx in xrange(rlo, rhi):
                for colx in xrange(clo, chi):
                    cell = self.sheet.cell(rowx, colx)
                    if rowx == rlo and colx == clo: xf_index = cell.xf_index
                    if cell.ctype == xlrd.XL_CELL_TEXT:
                        text_value += cell.value
                    else:
                        text_value += unicode(cell.value)
            dict_merged_cells[crange] = Cell(xlrd.XL_CELL_TEXT, text_value, xf_index=xf_index)

        return dict_merged_cells
