import xlrd

MISSING_VALUE_NOTATIONS = ['s', 'n.s', 'n.s.', '-', 'so', '.', 'ns', 'n', 'nd', 'ss', '', '///', 'n.d.', 'n.d', 'c']
"""
The string values that are used to substitue for missing values in Excel sheet
"""

class ExcelValidator():
    """
    Utility functions for validate data types of Excel columns, rows, cells
    """

    @classmethod
    def is_cell_empty_or_blank(cls, cell):
        return cell.ctype == xlrd.XL_CELL_EMPTY or cell.ctype == xlrd.XL_CELL_BLANK \
            or (cell.ctype == xlrd.XL_CELL_TEXT and cell.value.strip() == '')

    @classmethod
    def is_empty(cls, cells):
        for cell in cells:
            if not ExcelValidator.is_cell_empty_or_blank(cell): return False
        return True

    @classmethod
    def contains_text_cells(cls, row, first_column):
        for cell in row[first_column:]:
            if not ExcelValidator.contains_missing_value(cell) and not ExcelValidator.contains_numeric_value(cell) and cell.ctype == xlrd.XL_CELL_TEXT:
                return True
        return False

    @classmethod
    def contains_numeric_value(cls, cell):
        if cell.ctype == xlrd.XL_CELL_NUMBER:
            return True

        value = cell.value.replace('+', '') \
            if (type(cell.value) == str or type(cell.value) == unicode) \
            else cell.value

        try:
            float(value)
        except ValueError:
            return False

        return True

    @classmethod
    def contains_missing_value(cls, cell):
        return cell.ctype == xlrd.XL_CELL_TEXT and cell.value.lower().strip() in MISSING_VALUE_NOTATIONS
