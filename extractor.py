import codecs
import json
import os
import re
import sys

import xlrd

import file_utils
import utils
import variable_decoder as decoder
from boundary_finder import BoundaryFinder
from data_model import VariableSheet
from data_provider import DataProvider
from excel_utils import MISSING_VALUE_NOTATIONS, ExcelValidator
from log_utils import logger
from rdf_converter import RdfConverter
from table_extractor import TableExtractor
from config import ALL_EXCEL_FILES, ALL_TTL_FILES
from file_utils import csv_file_to_lines


def generate_all_excel_files_map():
    d = dict()
    for line in csv_file_to_lines(ALL_EXCEL_FILES):
        crawled_date, url, file_name, description, published_date = \
            utils.split_extractor_input_line(line)
        d[file_name] = (crawled_date, url, description, published_date)
    return d


all_excel_files_map = generate_all_excel_files_map()


class ExcelExtractor():
    """
    Extracting tables (`Table` instances) from an Excel file.
    Generating RDF file for each extracted table
    """

    def __init__(self, mode='multiple'):
        """
        Args:
            mode ('str'): extraction mode, the possible values are
                `"multiple"` for extracting multiple tables per sheet
                or `"single"` for extracting only one table per shee
        """
        self.variable_sheet = None
        self.mode = mode

    def find_sheet_of_list_variables(self):
        """
        Returns:
            the "List of variables" sheet which contains information to decode
            the encoded header cells in other sheets, the index of that sheet is
            also returned
        """
        for index, sheet_name in enumerate(self.book.sheet_names()):
            if sheet_name.lower() == 'liste des variables' or sheet_name.lower() == 'liste_variables':
                return index, self.book.sheet_by_name(sheet_name)

        return None, None

    def __extract_tables(self, list_table_metadata):
        """
        Args:
            list_table_metadata (list `TableMetadata`)
        Returns:
            list of extracted `Table`
        """
        results = list()
        for table_metadata in list_table_metadata:
            table_extractor = TableExtractor(
                self.data_provider,
                table_metadata,
                self.variable_sheet,
                self.book.xf_list
            )
            table = table_extractor.extract()
            if table:
                results.append(table)
        return results

    def extract_sheet(self, sheet_id):
        """
        Args:
            sheeet_id (`int`): the id of the given sheet
        Returns:
            list of extracted `Table` instanes
        """
        logger.info('Extract sheet ' + str(sheet_id))

        sheet = self.book.sheet_by_index(sheet_id)
        self.data_provider = DataProvider(sheet)
        boundary_finder = BoundaryFinder(sheet, self.data_provider)

        results = list()
        list_table_metadata = boundary_finder.next_list_table_metadata(
            self.mode)
        if self.mode == 'single':
            results.extend(self.__extract_tables(list_table_metadata))
        elif self.mode == 'multiple':
            while list_table_metadata:
                results.extend(self.__extract_tables(list_table_metadata))
                boundary_finder.post_extract(list_table_metadata)
                if not boundary_finder.contains_tables():
                    break
                list_table_metadata = boundary_finder.next_list_table_metadata(
                    self.mode)
        return results

    def extract_file(self, excel_file, sheet_idx=None):
        """
        Args:
            excel_file (`str`): path of an Excel file
            sheet_idx (list `int`): list of sheet's id from `excel_file`
        Returns:
            list of extracted `Table` instances
        """
        try:
            with xlrd.open_workbook(excel_file, on_demand=True, formatting_info=True) as book:
                logger.info(
                    self.mode + ' extracting Excel file: ' + excel_file)

                self.book = book

                if sheet_idx is None:
                    sheet_idx = range(self.book.nsheets)

                variables_sheet_index, variables_sheet = self.find_sheet_of_list_variables()
                if variables_sheet:
                    self.variable_sheet = decoder.build_list_varibles(
                        variables_sheet)

                results = list()
                for sheet_id in sheet_idx:
                    if sheet_id == variables_sheet_index:
                        continue

                    try:
                        results.append(self.extract_sheet(sheet_id))
                    except Exception as e:
                        logger.exception(
                            'ExtractingSheetError: %s, %s' % (repr(e), excel_file))

                return results
        except Exception as e:
            logger.exception('ReadingExcelFileError: ' + excel_file)

        return None

    def extract_and_save(self, file_path):
        """
        Generate RDF file for each extracted `Table` instance from the given `file_path` (`str`)
        """
        file_name = os.path.basename(file_path)
        file_id = file_name.replace('.xls', '')
        directory_prefix = self.mode + "_"
        extracted_folder = os.path.join(os.path.dirname(file_path),
                                        directory_prefix + file_id)

        # delete the previous extracted results (if they existed)
        file_utils.delete_folder_if_exists(extracted_folder)
        os.makedirs(extracted_folder)

        if file_path in all_excel_files_map:
            crawled_date, url, description, published_date = \
                all_excel_files_map[file_path]
        else:
            crawled_date, url, description, published_date = '', '', '', ''

        for sheet_id, tables in enumerate(self.extract_file(file_path)):
            for table_id, table in enumerate(tables):
                header_rows = table.header_zone_x.headers
                header_columns = table.header_zone_y.headers
                data_rows = table.data_zone.data_rows
                sheet_title = table.title
                sheet_comment = table.comment

                if header_rows and header_columns and data_rows:
                    try:
                        sheet_directory = os.path.join(
                            extracted_folder, str(sheet_id))
                        if not os.path.exists(sheet_directory):
                            os.makedirs(sheet_directory)

                        ttl_file_path = os.path.join(
                            sheet_directory, str(table_id) + ".ttl")
                        rdf_converter = RdfConverter(ttl_file_path)
                        _file = rdf_converter.write_file(
                            file_name,
                            crawled_date, url, description,
                            file_path)

                        sheet = rdf_converter.write_sheet(sheet_id,
                                                          sheet_title if sheet_title else '',
                                                          sheet_comment if sheet_comment else '',
                                                          published_date)
                        rdf_converter.generate_triples(
                            sheet,
                            data_rows,
                            header_columns,
                            header_rows
                        )

                        with open(ALL_TTL_FILES, 'a+') as f:
                            f.write('{}\n'.format(ttl_file_path))
                    except Exception as e:
                        logger.exception(
                            'RdfConverterError: %s, %s' % (repr(e), file_path))

        logger.info(self.mode + ' extraction done: ' + file_path)


if __name__ == '__main__':
    extrator = ExcelExtractor(mode=sys.argv[2])
    extrator.extract_and_save(file_path=sys.argv[1])
