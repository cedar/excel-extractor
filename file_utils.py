import shutil
import codecs
import json
import os

import log_utils

"""
Utility functions for file-related tasks
"""

SYSTEM_FOLDERS = ['.DS_Store', '__MACOSX']
"""
Name of system folders that we should get rid of when listing files from a directory
"""

def list_directory(path):
    files = os.listdir(path)
    for system_folder in SYSTEM_FOLDERS:
        if system_folder in files:
            files.remove(system_folder)
    return files

def list_files(path, extension):
    files = list()

    for child in list_directory(path):
        full_path = os.path.join(path, child)
        if os.path.isfile(full_path):
            if extension in full_path: files.append(full_path)
        elif os.path.isdir(full_path):
            files.extend(list_files(full_path, extension))

    return files

def delete_folder_if_exists(folder_path):
    shutil.rmtree(folder_path, ignore_errors=True)

def csv_file_to_lines(csv_file, skip_header=True):
    with codecs.open(csv_file, encoding='utf-8') as f:
        lines = [file_name.replace('\n', '') for file_name in f.readlines()]
        if skip_header:
            return lines[1:]
        else:
            return lines
