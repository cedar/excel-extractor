import logging

"""
Logger with date time formated
"""

def get_log(file_name):
    log_name = file_name[:file_name.index('.')]
    logger = logging.getLogger(log_name)

    logging.basicConfig(level=logging.INFO)
    handler = logging.FileHandler(file_name)
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
    handler.setFormatter(formatter)
    logger.addHandler(handler)

    return logger

logger = get_log('extractor.log')
