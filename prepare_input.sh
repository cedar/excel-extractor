insee_data_folder='/data/tcao/insee'
xls_extractor_input_csv='/home/cedar/tcao/insee-crawler/xls_extractor_input.csv'
all_excel_files_csv='all_excel_files.csv'

python main.py --extract-zip $insee_data_folder
python main.py --list-excel $insee_data_folder $xls_extractor_input_csv $all_excel_files_csv
