import codecs

from utils import force_encode, clean_empty_spaces, hash_code

PREFIX = 'inseeXtr'
"""
Prefix of generated RDF file
"""
NAMESPACE_URI = 'http://inseeXtr.excel/'
"""
Namespace of generated RDF file
"""


def uri(_type, _id):
    return "%s:%s_%s" % (PREFIX, _type, str(_id))


def type_uri(_type):
    return "%s:%s" % (PREFIX, _type)


def predicate(name):
    return "%s:%s" % (PREFIX, name)


def get_uri_prefix(uri):
    return uri.split(':')[1]


def normalized_text(text):
    """
    Clean the double quotes from `text` (`str`).
    Also remove the new line characters.
    """
    return clean_empty_spaces(text.replace('"', ' '))


class RdfConverter():
    """
    Generate RDF file from extracted `Table` instance
    """

    def __init__(self, output_file):
        """
        Args:
            output_file (`str`): the file path where the generated RDF file will be stored
        """
        self.output_file = output_file

    def write_header_cell(self, f, header_uri, cell_value, hierarchy_type,
                          aggregated_cell_uri):
        f.write('%s %s %s ;\n' % (
            header_uri,
            predicate('value'),
            '"%s"' % normalized_text(cell_value)
        ))
        f.write('\t\t%s %s ;\n' % (
            predicate('belongs_to'),
            self.sheet_uri
        ))
        header_type = type_uri('HeaderRow' if hierarchy_type ==
                               'YHierarchy' else 'HeaderColumn')
        if aggregated_cell_uri:
            f.write('\t\t%s %s ;\n' % (
                'rdf:type',
                header_type
            ))
            f.write('\t\t%s %s .\n\n' % (
                predicate(hierarchy_type),
                aggregated_cell_uri
            ))
        else:
            f.write('\t\t%s %s .\n\n' % (
                'rdf:type',
                header_type
            ))

    def write_data_cell(self, f, _id, cell_value, x, y, header_x_uri, header_y_uri):
        f.write('%s %s %s ;\n' % (
            uri(get_uri_prefix(self.sheet_uri) + '_' + 'DC', _id),
            predicate('value'),
            '"%s"' % cell_value if cell_value is not None else '"None"'
        ))
        f.write('\t\t%s %s ;\n' % (
            predicate('belongs_to'),
            self.sheet_uri
        ))
        f.write('\t\t%s %s ;\n' % (
            'rdf:type',
            type_uri('DataCell')
        ))
        f.write('\t\t%s %s ;\n' % (
            predicate('posX'),
            x
        ))
        f.write('\t\t%s %s ;\n' % (
            predicate('posY'),
            y
        ))
        f.write('\t\t%s %s ;\n' % (
            predicate('closestXCell'),
            header_x_uri
        ))
        f.write('\t\t%s %s .\n\n' % (
            predicate('closestYCell'),
            header_y_uri
        ))

    def write_sheet(self, sheet_id, sheet_title, comment, published_date):
        with codecs.open(self.output_file, 'a+', encoding='utf-8') as f:
            sheet_uri = uri('S', hash_code(
                self.output_file) + '_' + str(sheet_id))
            f.write('%s %s %s ;\n' % (
                sheet_uri,
                predicate('title'),
                force_encode('"%s"' % normalized_text(sheet_title))
            ))
            f.write('\t\t%s %s ;\n' % (
                'rdf:type',
                type_uri('Sheet')
            ))
            f.write('\t\t%s %s ;\n' % (
                predicate('comment'),
                force_encode('"%s"' % normalized_text(comment))
            ))
            f.write('\t\t%s %s ;\n' % (
                predicate('ttl_file_path'),
                '"%s"' % self.output_file
            ))
            f.write('\t\t%s %s ;\n' % (
                predicate('published_date'),
                '"%s"' % published_date
            ))
            f.write('\t\t%s %s .\n\n' % (
                predicate('belongs_to'),
                self.file_uri
            ))

            self.sheet_uri = sheet_uri

    def write_file(self, file_name, crawled_date, url,
                   file_description, downloaded_file_path):
        with codecs.open(self.output_file, 'w', encoding='utf-8') as f:
            f.write("@prefix %s: <%s> .\n" % (PREFIX, NAMESPACE_URI))
            f.write(
                "@prefix rdf:  <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .\n")
            f.write(
                "@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .\n\n")

            f.write("%s:YHierarchy rdfs:range %s:HeaderRow ;\n" %
                    (PREFIX, PREFIX))
            f.write("\t\trdfs:domain %s:HeaderRow .\n" % PREFIX)
            f.write("%s:XHierarchy rdfs:range %s:HeaderColumn ;\n" %
                    (PREFIX, PREFIX))
            f.write("\t\trdfs:domain %s:HeaderColumn .\n" % PREFIX)

            f.write("%s:closestYHeaderCell rdfs:range %s:DataCell ;\n" %
                    (PREFIX, PREFIX))
            f.write("\t\trdfs:domain %s:HeaderRow .\n" % PREFIX)
            f.write("%s:closestXHeaderCell rdfs:range %s:DataCell ;\n" %
                    (PREFIX, PREFIX))
            f.write("\t\trdfs:domain %s:HeaderColumn .\n" % PREFIX)

            f.write("%s:belongs_to rdfs:range %s:Sheet ;\n" %
                    (PREFIX, PREFIX))
            f.write("\t\trdfs:domain %s:Dataset .\n" % PREFIX)

            f.write("%s:belongs_to rdfs:range %s:HeaderColumn ;\n" %
                    (PREFIX, PREFIX))
            f.write("\t\trdfs:domain %s:Sheet .\n" % PREFIX)

            f.write("%s:belongs_to rdfs:range %s:HeaderRow ;\n" %
                    (PREFIX, PREFIX))
            f.write("\t\trdfs:domain %s:Sheet .\n" % PREFIX)

            f.write("%s:belongs_to rdfs:range %s:DataCell ;\n" %
                    (PREFIX, PREFIX))
            f.write("\t\trdfs:domain %s:Sheet .\n\n" % PREFIX)

            file_uri = uri('F', hash_code(downloaded_file_path))
            f.write('%s %s %s ;\n' % (
                file_uri,
                'rdf:type',
                type_uri('Dataset')
            ))
            f.write('\t\t%s %s ;\n' % (
                predicate('downloaded_date'),
                '"%s"' % crawled_date
            ))
            f.write('\t\t%s %s ;\n' % (
                predicate('url'),
                '"%s"' % url
            ))
            f.write('\t\t%s %s ;\n' % (
                predicate('downloaded_file_path'),
                '"%s"' % downloaded_file_path
            ))
            f.write('\t\t%s %s .\n\n' % (
                predicate('description'),
                '"%s"' % normalized_text(file_description)
            ))

            self.file_uri = file_uri

    def __generate_header_uris(self, header_rows, cell_prefix):
        cell_prefix = get_uri_prefix(self.sheet_uri) + '_' + cell_prefix

        rows = map(list, zip(*header_rows))
        uris = list()
        header_cell_id = 0
        for layer, row in enumerate(rows):
            uris.append(list())
            row_uris = uris[-1]
            for index, cell_value in enumerate(row):
                # a merged cell from layer #1 upward
                if layer > 0 and index - 1 >= 0 and cell_value == row[index - 1]:
                    # reuse the uri of previous cell
                    row_uris.append(uri(cell_prefix, header_cell_id))
                else:
                    header_cell_id += 1
                    # create a new uri
                    row_uris.append(uri(cell_prefix, header_cell_id))
        return uris

    def __generate_header_cells(self, f, header_rows, sheet_uri, cell_type):
        first_layer_uris = list()

        hierachy = 'YHierarchy' if cell_type == 'HeaderRow' else 'XHierarchy'
        cell_prefix = 'HR' if cell_type == 'HeaderRow' else 'HC'

        rows = map(list, zip(*header_rows))
        uris = self.__generate_header_uris(header_rows, cell_prefix)
        for layer, t in enumerate(zip(rows, uris)):
            previous_uri = None
            for row_id, (cell_value, uri) in enumerate(zip(*t)):
                if uri == previous_uri:
                    continue

                if layer >= 0 and layer < len(rows) - 1:
                    aggregated_cell_uri = uris[layer + 1][row_id]
                else:
                    aggregated_cell_uri = None

                self.write_header_cell(f, uri, cell_value,
                                       hierachy, aggregated_cell_uri)

                if layer == 0:
                    first_layer_uris.append(uri)

                previous_uri = uri
        return first_layer_uris

    def generate_triples(self, sheet_uri, data_rows, header_columns, header_rows):
        if len(data_rows[0]) != len(header_rows):
            return

        with codecs.open(self.output_file, 'a+', encoding='utf-8') as f:
            first_header_row_uris = self.__generate_header_cells(f,
                                                                 header_rows, sheet_uri, 'HeaderRow')

            if len(first_header_row_uris) != len(data_rows[0]):
                return

            first_header_column_uris = self.__generate_header_cells(f,
                                                                    header_columns, sheet_uri,
                                                                    'HeaderColumn')

            data_cell_id = 0
            for x, data_row in enumerate(data_rows):
                for y, cell_value in enumerate(data_row):
                    self.write_data_cell(f, data_cell_id, cell_value,
                                         x, y, first_header_column_uris[x], first_header_row_uris[y])
                    data_cell_id += 1


if __name__ == '__main__':
    rdf_converter = RdfConverter('test.ttl')
    file_uri = rdf_converter.write_file(
        'file_name', 'downloaded_date', 'url', 'description', 'downloaded_file_path')
    sheet_uri = rdf_converter.write_sheet(
        0, 'sheet_title', 'comment', 'published_date')
    rdf_converter.generate_triples(
        sheet_uri,
        [['1', '2'], ['3', '4']],
        [['a1', 'a2'], ['b1', 'b2']],
        [['x1'], ['x2']]
    )
