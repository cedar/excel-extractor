import json

import xlrd
from xlrd.sheet import Cell

import utils
import variable_decoder as decoder
from config import *
from data_model import (DataZone, HeaderZone, Table, TableMetadata,
                        VariableSheet)
from data_provider import DataProvider
from excel_utils import MISSING_VALUE_NOTATIONS, ExcelValidator
from log_utils import logger


class BaseExtractor(object):
    """
    Base class of `TableExtractor`, `DataZoneExtractor` and `HeaderZoneExtractor`
    """

    def __init__(self, data_provider, table_metadata):
        """
        Args:
            data_provider (`DataProvider`)
            table_metadata (`TableMetadata`)
        """
        self.table_metadata = table_metadata
        self.data_provider = data_provider

    def get_row(self, row_id):
        """
        Returns:
            collection of cells from row which has ``row_id`` and restricted in
            the left and right border of ``table_metadata``
        """
        return self.data_provider.get_row(row_id)[self.table_metadata.first_column: self.table_metadata.last_column]

class TableExtractor(BaseExtractor):
    """
    Extracting `Table` from `TableMetadata`
    """

    def __init__(self, data_provider, table_metadata, variable_sheet, xf_list):
        """
        Args:
            variable_sheet (`VariableSheet`): used to decode the encoded header cells
            xf_list: A list of `XF` class instances, each corresponding to an `XF` record,
                obtained from `book.xf_list` (`book` is an instance of `xlrd.Book`),
                used to identify border of header zone
        """
        super(TableExtractor, self).__init__(data_provider, table_metadata)

        self.has_encoded_header = False
        self.list_variables = None
        self.code_name_definions = None
        self.first_data_row_idx = None

        self.xf_list = xf_list
        self.variable_sheet = variable_sheet

    def __extract_table_title(self, header_rows_border):
        """
        Args:
            header_rows_border (`int` or `None`): the border of header zone (if available)
        Returns:
            (string): title of table (located above the header zone)
        """
        title = ''
        if header_rows_border is not None:
            for row_id in xrange(self.table_metadata.range_of_rows[0], header_rows_border + 1):
                row = self.get_row(row_id)
                title += ' '.join([DataProvider.cell_value(cell) for cell in row])
        if title == '':
            title = DataProvider.cell_value(self.data_provider.get_merged_cell(
                row_id=self.table_metadata.range_of_rows[0],
                col_id=self.table_metadata.first_column
            ))
        return title

    def __extract_table_comment(self, data_zone):
        """
        Returns:
            (string): comment of table (located at the bottom of table)
        """
        comment = ''
        if data_zone.last_row:
            for row_id in xrange(data_zone.last_row + 1,
                                 self.table_metadata.range_of_rows[-1] + 1):
                row = self.get_row(row_id)
                comment += ' '.join([DataProvider.cell_value(cell) for cell in row])
        return comment

    def extract(self):
        """
        Returns:
            a `Table` object
        """
        logger.debug('Extracting from TableMetadata: ' + str(self.table_metadata))

        table = Table()

        if self.data_provider.get_row_count() > 0:
            if not self.table_metadata.first_column_of_data_cells:
                table = None
            else:
                data_zone = DataZoneExtractor(self.data_provider, self.table_metadata).extract()

                if data_zone.data_rows:
                    header_zone_extractor = HeaderZoneExtractor(self.xf_list,
                        self.table_metadata, data_zone,
                        self.data_provider, self.variable_sheet)
                    header_zone_x, header_zone_y, data_zone = header_zone_extractor.extract()

                    table_title = self.__extract_table_title(
                         header_zone_extractor.header_rows_border)
                    table_comment = self.__extract_table_comment(data_zone)

                    table = Table(header_zone_x, header_zone_y, data_zone,
                                  table_title, table_comment)
                    logger.debug('[EXTRACTED A TABLE]')
                    logger.debug('\theader rows: ' + str(header_zone_x.headers))
                    logger.debug('\theader columns: ' + str(header_zone_y.headers))
                else:
                    table = None

        return table

DATA_ROW_NEIGHBORS = [-2, -1, 1, 2]
"""
The distances from a data row to potential data rows
"""
def search_irregular_ids(current_ids, list_numeric_row_ids):
    """
    Args:
        current_ids (list `int`): "official" row ids of data rows
        list_numeric_row_ids (list `int`): list of row ids that contain numeric cells
    Returns:
        list of row's id (`int`) from potential data rows
            + they are separated with other data rows by empty rows
            + they contain several special cells which them don't have the same signatures of standard data rows
    """
    new_ids = list()
    for row_id in current_ids:
        for offset in DATA_ROW_NEIGHBORS:
            new_id = row_id + offset
            if new_id in list_numeric_row_ids:
                new_ids.append(new_id)
                list_numeric_row_ids.remove(new_id)
                break
    if new_ids:
        return search_irregular_ids(current_ids + new_ids, list_numeric_row_ids) + new_ids
    return new_ids

class DataZoneExtractor(BaseExtractor):
    """
    Extracting `DataZone` from a `TableMetadata`
    """

    def __build_list_row_stats(self):
        """
        Returns:
            a list of row's data types (the information of
            how many cells that share the same type in a row)
        """
        list_row_data_types = list()
        dict_rows_signatures = self.table_metadata.dict_rows_signatures
        for rx in self.table_metadata.range_of_rows:
            if rx in dict_rows_signatures:
                row = self.get_row(rx)
                types = dict()
                for ctype in dict_rows_signatures[rx]:
                    if ctype not in types:
                        types[ctype] = 0
                    types[ctype] += 1
                list_row_data_types.append(types)
        return list_row_data_types

    def __cluster_row_data_types(self, list_row_data_types):
        """
        Combine rows with the same data types into cluster
        """
        clusters = dict()
        for row_id, types in zip(self.table_metadata.range_of_rows, list_row_data_types):
            key = json.dumps(types)
            if key not in clusters:
                clusters[key] = list()
            clusters[key].append(row_id)

        return clusters

    def __find_data_row_ids(self, row_data_type_clusters):
        """
        Returns:
            list of row's id, these rows contain data (numeric values)
        """
        candidate_types = dict()
        list_numeric_row_ids = list()
        number_key = str(xlrd.XL_CELL_NUMBER)

        for types, row_ids in row_data_type_clusters.iteritems():
            dict_types = json.loads(types)
            if number_key in dict_types:
                candidate_types[types] = len(row_ids)
                for row_id in row_ids:
                    list_numeric_row_ids.append(row_id)

        # main cell types of data rows
        main_data_row_types = max(candidate_types, key=candidate_types.get)
        data_row_ids = row_data_type_clusters[main_data_row_types]

        list_numeric_row_ids = [row_id for row_id in list_numeric_row_ids if row_id not in data_row_ids]
        return sorted(data_row_ids + search_irregular_ids(data_row_ids, list_numeric_row_ids))

    def __clean_up_empty_columns(self, data_rows):
        """
        Make sure that there's no empty columns in data zone and horizontal header zone
        """
        columns = map(list, zip(*data_rows))

        last_column_id = len(columns)
        while last_column_id - 1 >= 0:
            unique_values = set(columns[last_column_id - 1])
            if unique_values == set([None]) or unique_values == set(['']):
                last_column_id -= 1
            else:
                break

        if last_column_id < len(columns):
            self.table_metadata.last_column = self.table_metadata.first_column_of_data_cells + last_column_id

        return [row[:last_column_id] for row in data_rows]

    def __ensure_data_row(self, row, row_id):
        """
        Make sure that the given row contain data cells
        """
        # data row shouldn't contain text cells
        if ExcelValidator.contains_text_cells(row, self.table_metadata.first_column_of_data_cells):
            return False

        # there must be at least 1 header cell on the left of the first data cell
        has_header_cells = False
        for col_id in reversed(xrange(self.table_metadata.first_column, self.table_metadata.first_column_of_data_cells)):
            cell = self.data_provider.get_merged_cell(row_id, col_id)
            if not ExcelValidator.is_cell_empty_or_blank(cell):
                has_header_cells = True
                break
        return has_header_cells

    def extract(self):
        """
        Returns:
            a `DataZone` from ``table_metadata``
        """
        data_zone = DataZone(data_rows=list(), data_row_idx=list())

        list_row_data_types = self.__build_list_row_stats()
        has_data_rows = False
        for dict_types in list_row_data_types:
            if str(xlrd.XL_CELL_NUMBER) in dict_types.keys():
                has_data_rows = True
                break

        if has_data_rows:
            row_data_type_clusters = self.__cluster_row_data_types(list_row_data_types)
            logger.debug('\tRow clusters: ' + str(row_data_type_clusters))

            data_row_ids = self.__find_data_row_ids(row_data_type_clusters)
            logger.debug('\tPotential data row ids: ' + str(data_row_ids))

            first_index = self.table_metadata.first_column_of_data_cells - self.table_metadata.first_column
            for rx in sorted(data_row_ids):
                row = self.get_row(rx)

                if self.__ensure_data_row(row, rx):
                    data_zone.data_row_idx.append(rx)

                    data_zone.data_rows.append(
                        [DataProvider.data_cell_value(cell) for cell in row[first_index:]]
                    )

                    if data_zone.first_row is None:
                        data_zone.first_row = rx
                        logger.debug('\tFirst row of data cells: ' + str(data_zone.first_row))

                    data_zone.last_row = rx

        logger.debug('\tData row ids: ' + str(data_zone.data_row_idx))

        data_zone.data_rows = self.__clean_up_empty_columns(data_zone.data_rows)
        return data_zone

class HeaderZoneExtractor(BaseExtractor):
    """
    Extracting `HeaderZone` from a `TableMetadata`
    """
    def __init__(self, xf_list, table_metadata, data_zone, data_provider, variable_sheet):
        """
        Args:
            data_zone (`DataZone`)
            variable_sheet (`VariableSheet`): used to decode the encoded header cells
            xf_list: A list of `XF` class instances, each corresponding to an `XF` record,
                obtained from `book.xf_list` (`book` is an instance of `xlrd.Book`),
                used to identify border of header zone
        """
        super(HeaderZoneExtractor, self).__init__(data_provider, table_metadata)

        self.xf_list = xf_list
        self.data_zone = data_zone
        self.variable_sheet = variable_sheet

        self.first_column_of_data_cells = table_metadata.first_column_of_data_cells
        self.last_column_idx = table_metadata.last_column
        self.first_data_row_idx = data_zone.first_row

    def extract(self):
        """
        Returns:
            a `HeaderZone` for x axis and a `HeaderZone` for y axis
        """
        range_of_rows = self.table_metadata.range_of_rows

        # Identify header row(s)
        border = self.__find_top_border(top_row=range_of_rows[0])
        header_rows = self.extract_header_rows(border, top_row=range_of_rows[0])

        empty_header_rows = True
        for header_row in header_rows:
            if header_row:
                empty_header_rows = False
        if empty_header_rows:
            header_rows = [[str(cell)] for cell in self.data_zone.data_rows[0]]
            self.data_zone.data_rows = self.data_zone.data_rows[1:]
            self.data_zone.data_row_idx = self.data_zone.data_row_idx[1:]

        empty_column_idx = list()
        for index, header_row in enumerate(header_rows):
            if header_row == ['None'] or header_row == [] or header_row == None:
                empty_column_idx.append(index)
        if empty_column_idx:
            def clean_empty_cells(row):
                return [cell for index, cell in enumerate(row) if index not in empty_column_idx]
            header_rows = clean_empty_cells(header_rows)
            self.data_zone.data_rows = [clean_empty_cells(row) for row in self.data_zone.data_rows]

        # Identify header column(s)
        header_columns = self.extract_header_columns(self.data_zone.data_row_idx)

        return HeaderZone(header_rows, 'x'), HeaderZone(header_columns, 'y'), self.data_zone

    def extract_header_columns(self, data_row_ids):
        """
        Args:
            data_row_ids (list `int`): list of row's id
        Returns:
            list of header cell values (`str`) of the vertical headers
        """
        all_headers = list()
        for row_id in data_row_ids:
            headers = list()
            for col_id in reversed(xrange(self.table_metadata.first_column, self.first_column_of_data_cells)):
                cell = self.data_provider.get_merged_cell(row_id, col_id)
                value = DataProvider.cell_value(cell)
                if value != '':
                    headers.append(value)
            all_headers.append(headers)

        return all_headers

    def extract_header_rows(self, border, top_row):
        """
        Args:
            border (`int` or `None`): the top border of header zone (if available)
            top_row (`int`): the top row id given by `table_metadata`
        Returns:
            list of header cell values (`str`) of the horizontal headers
        """
        logger.debug("\tHeader row's border: " + str(border))

        header_rows = list()
        if border:
            self.header_rows_border = border

            last_column = self.table_metadata.last_column
            previous_header_borders, previous_headers = [], []
            for column_id in xrange(self.first_column_of_data_cells, last_column):
                headers, header_borders = self.__find_headers_inside_borders(self.first_data_row_idx - 1, column_id, \
                    border, previous_header_borders, previous_headers)
                previous_header_borders = header_borders
                previous_headers = headers

                header_rows.append(headers)
        else:
            idx = self.first_data_row_idx
            while idx > top_row:
                idx -= 1
                row = self.get_row(idx)
                first_index = self.first_column_of_data_cells - self.table_metadata.first_column
                first_cell = row[first_index]
                if not ExcelValidator.is_cell_empty_or_blank(first_cell):
                    header_row = self.__get_header_row(idx)
                    header_rows.append(self.__transform_merged_header_row(header_row))

                    # encoded header rows should have only 1 row
                    if self.variable_sheet and self.variable_sheet.has_encoded_header():
                        break

            self.header_rows_border = idx

            header_rows = map(list, zip(*header_rows))

        return header_rows

    def __transform_merged_header_row(self, row):
        transformed_row = row

        last_header_cell_value = None
        first_index = self.first_column_of_data_cells - self.table_metadata.first_column
        for ix, cell in enumerate(row[first_index:]):
            if cell.ctype == xlrd.XL_CELL_TEXT:
                last_header_cell_value = cell.value

            if cell.ctype == xlrd.XL_CELL_EMPTY:
                if last_header_cell_value is not None:
                    transformed_row[ix + first_index] = Cell(xlrd.XL_CELL_TEXT, last_header_cell_value)

        if self.variable_sheet and self.variable_sheet.has_encoded_header():
            return [decoder.decode(utils.clean_empty_spaces(cell.value),
                self.variable_sheet.list_variables,
                self.variable_sheet.code_name_definions)
                for cell in transformed_row[first_index:]]
        else:
            return [DataProvider.cell_value(cell) for cell in transformed_row[first_index:]]

    def __get_header_row(self, row_id):
        header_row = self.get_row(row_id)
        normalized_header_row = header_row
        for col_id, cell in enumerate(header_row):
            normalized_header_row[col_id] = self.data_provider.get_merged_cell(row_id, col_id + self.table_metadata.first_column)
        return normalized_header_row

    def __find_top_border(self, top_row):
        def almost_a_border(borders):
            return sum([1 for border_value in borders if border_value > 0]) >= len(borders) - 1 \
                or not (0 in borders)

        row_id = top_row + 1
        border = None
        while row_id <= self.first_data_row_idx:
            cells = self.data_provider.get_row(row_id)[self.first_column_of_data_cells: self.last_column_idx]

            if row_id + 1 > self.data_provider.get_row_count() - 1:
                break

            bellow_cells = self.data_provider.get_row(row_id + 1)[self.first_column_of_data_cells: self.last_column_idx]

            bottom_borders = [self.xf_list[cell.xf_index].border.bottom_line_style for cell in cells]
            top_borders = [self.xf_list[cell.xf_index].border.top_line_style for cell in bellow_cells]
            borders = [x + y for x, y in zip(bottom_borders, top_borders)]

            if almost_a_border(borders):
                border = row_id
                break

            row_id += 1

        return border

    def __find_headers_inside_borders(self, starting_row_id, column_id, header_border, \
        previous_header_borders, previous_headers):
        headers = list()
        header_borders = list()
        row_id = starting_row_id
        merged_header_cell_text = ''

        previous_cell = None
        while row_id >= header_border:
            cell = self.data_provider.get_merged_cell(row_id, column_id)
            if cell == previous_cell:
                row_id -= 1
                continue
            previous_cell = cell

            if type(cell.value) == str:
                cell_value = cell.value.encode('utf-8')
            elif type(cell.value) == unicode:
                cell_value = cell.value
            else:
                cell_value = str(cell.value)
            merged_header_cell_text = cell_value + ' ' + merged_header_cell_text

            reach_border = False
            if row_id - 1 >= 0:
                above_cell = self.data_provider.cell(row_id - 1, column_id)
                reach_border = self.xf_list[above_cell.xf_index].border.bottom_line_style != 0

            reach_border = reach_border or self.xf_list[cell.xf_index].border.top_line_style != 0
            if reach_border:
                header = utils.clean_empty_spaces(merged_header_cell_text.strip())
                headers.append(header)

                header_borders.append(row_id)
                merged_header_cell_text = ''

            row_id -= 1

        if header_borders == previous_header_borders:
            if len(headers) > 0:
                headers = [headers[0]] + [header for header in headers[1:] if header != '']
            if len(headers) < len(previous_headers):
                headers.extend(previous_headers[len(headers):])

        return [header for header in headers if header != ''], header_borders
