# -*- coding: UTF-8 -*-


import os
from base_test import template_test, template_test_header_rows
from extractor import ExcelExtractor


def test_aggregated_header_rows():
    template_test('test_data/test1.xls',
        [[u'nombre', u'nombre', u"en milions d'euros", u"en milions d'euros", u"en milions d'euros", u'en %', u'en %'], [u'Entreprises', u'Salari\xe9s', u"Chiffre d'affaires", u'Valeur ajout\xe9e (VA)', u'Investissement', u'Part de la VA', u'Taux de marge']],
        [[u'Extraction, \xe9nergie, environnement', u"Fabrication d'aliments, tabac"]],
        [[1535.0, 2025.0, 600.0, 205.0, 157.0, 3.3, 55.7], [700.0, 4720.0, 1397.0, 429.0, 83.0, 6.9, 29.7]]
    )


def test_missing_value_in_data_cells():
    template_test('test_data/test1b.xls',
        [[u'nombre', u'nombre', u"en milions d'euros", u"en milions d'euros", u"en milions d'euros", u'en %', u'en %'], [u'Entreprises', u'Salari\xe9s', u"Chiffre d'affaires", u'Valeur ajout\xe9e (VA)', u'Investissement', u'Part de la VA', u'Taux de marge']],
        [[u'Extraction, \xe9nergie, environnement', u"Fabrication d'aliments, tabac"]],
        [[1535.0, 2025.0, 600.0, 205.0, None, 3.3, 55.7], [700.0, 4720.0, 1397.0, 429.0, 83.0, 6.9, 29.7]]
    )


def test_missing_value_in_the_first_column():
    template_test('test_data/test1c.xls',
        [[u'nombre', u'nombre', u"en milions d'euros", u"en milions d'euros", u"en milions d'euros", u'en %', u'en %'], [u'Entreprises', u'Salari\xe9s', u"Chiffre d'affaires", u'Valeur ajout\xe9e (VA)', u'Investissement', u'Part de la VA', u'Taux de marge']],
        [[u'Extraction, \xe9nergie, environnement', u"Fabrication d'aliments, tabac"]],
        [[1535.0, 2025.0, 600.0, 205.0, 1, 3.3, 55.7], [None, 4720.0, 1397.0, 429.0, 83.0, 6.9, 29.7]]
    )


def test_missing_values_in_data_cells():
    template_test('test_data/test1d.xls',
        [[u'nombre', u'nombre', u"en milions d'euros", u"en milions d'euros", u"en milions d'euros", u'en %', u'en %'], [u'Entreprises', u'Salari\xe9s', u"Chiffre d'affaires", u'Valeur ajout\xe9e (VA)', u'Investissement', u'Part de la VA', u'Taux de marge']],
        [[u'Extraction, \xe9nergie, environnement', u"Fabrication d'aliments, tabac"]],
        [[1535.0, 2025.0, None, 205.0, 1, 3.3, 55.7], [None, 4720.0, 1397.0, 429.0, 83.0, 6.9, None]]
    )


def test_aggregated_header_rows_and_columns():
    template_test('test_data/test2.xls',
        [[u'Tous niveaux', u'dont A', u'dont B', u'dont C', u'Tous niveaux', u'dont A', u'dont B', u'dont C', u'Tous niveaux', u'dont A', u'dont B', u'dont C'], [u'Hommes & Femmes', u'Hommes & Femmes', u'Hommes & Femmes', u'Hommes & Femmes', u'Hommes', u'Hommes', u'Hommes', u'Hommes', u'Femmes', u'Femmes', u'Femmes', u'Femmes']],
        [[u'Total', u'Titulaires', u'Non titulaires', u'Total', u'Titulaires', u'Non titulaires'], [u'Tous minist\xe8res', u'Tous minist\xe8res', u'Tous minist\xe8res', u'Affaires \xe9trang\xe8res', u'Affaires \xe9trang\xe8res', u'Affaires \xe9trang\xe8res']],
        [[41.0, 43.0, 40.0, 42.0, 41.0, 44.0, 39.0, 39.0, 42.0, 42.0, 41.0, 44.0], [42.0, 43.0, 41.0, 42.0, 42.0, 44.0, 40.0, 39.0, 43.0, 41.0, 44.0, 45.0], [39.0, 42.0, 35.0, 39.0, 39.0, 42.0, 35.0, 39.0, 39.0, 42.0, 35.0, 40.0], [46.0, 47.0, 45.0, 46.0, 47.0, 48.0, 45.0, 46.0, 45.0, 45.0, 45.0, 46.0], [47.0, 47.0, 45.0, 45.0, 47.0, 48.0, 45.0, 44.0, 46.0, 46.0, 47.0, 46.0], [44.0, 46.0, 44.0, 47.0, 46.0, 47.0, 45.0, 48.0, 43.0, 44.0, 44.0, 46.0]]
    )


def test_numeric_column_in_header_columns():
    template_test('test_data/test3.xls',
        [[u'Total', u'Industrie (B-E)', u'Construction (F)', u'Services divers (G-N sauf J,K,M)', u'Services TIC (J)', u'Services professionnels (M)', u'Total', u'Industrie (B-E)', u'Construction (F)', u'Services divers (G-N sauf J,K,M)', u'Services TIC (J)', u'Services professionnels (M)'], [u'Total', u'Total', u'Total', u'Total', u'Total', u'Total', u'Gazelles', u'Gazelles', u'Gazelles', u'Gazelles', u'Gazelles', u'Gazelles']],
        [[u'Total', u'Succ\xe8s complet', u'Echec partiel ou complet', u'Total', u'Succ\xe8s complet', u'Echec partiel ou complet', u'Total', u'Succ\xe8s complet', u'Echec partiel ou complet', u'Total', u'Succ\xe8s complet', u'Echec partiel ou complet'], [u'Cr\xe9dit-bail, location-vente (leasing)', u'Cr\xe9dit-bail, location-vente (leasing)', u'Cr\xe9dit-bail, location-vente (leasing)', u'Affacturage', u'Affacturage', u'Affacturage', u'Cr\xe9dit-bail, location-vente (leasing)', u'Cr\xe9dit-bail, location-vente (leasing)', u'Cr\xe9dit-bail, location-vente (leasing)', u'Affacturage', u'Affacturage', u'Affacturage'], [u'2007', u'2007', u'2007', u'2007', u'2007', u'2007', u'2010', u'2010', u'2010', u'2010', u'2010', u'2010']],
        [[14.08, 3.25, 4.13, 5.44, 0.27, 0.99, 0.19, 0.04, 0.05, 0.08, 0.01, 0.02], [13.59, 3.11, 4.03, 5.24, 0.25, 0.96, 0.18, 0.04, 0.05, 0.07, 0.01, 0.02], [0.48, 0.14, 0.1, 0.2, 0.02, 0.03, 0.01, 0.0, 0.0, 0.01, 0.0, 0.0], [1.89, 0.6, 0.39, 0.67, 0.09, 0.14, 0.03, 0.01, 0.01, 0.01, 0.0, 0.01], [1.68, 0.54, 0.3, 0.63, 0.08, 0.12, 0.03, 0.01, 0.0, 0.01, 0.0, 0.01], [0.21, 0.05, 0.09, 0.04, 0.01, 0.02, 0.01, 0.0, 0.0, 0.0, 0.0, 0.0], [16.9, 3.75, 4.69, 6.88, 0.33, 1.26, 0.21, 0.04, 0.04, 0.09, 0.01, 0.02], [15.59, 3.31, 4.49, 6.33, 0.29, 1.17, 0.18, 0.04, 0.04, 0.08, 0.01, 0.01], [1.32, 0.43, 0.2, 0.55, 0.04, 0.1, 0.03, 0.0, 0.01, 0.01, 0.0, 0.0], [3.44, 1.13, 0.89, 0.95, 0.2, 0.27, 0.07, 0.02, 0.01, 0.03, 0.01, 0.01], [2.48, 0.84, 0.64, 0.69, 0.13, 0.18, 0.06, 0.01, 0.0, 0.03, 0.01, 0.01], [0.95, 0.29, 0.26, 0.26, 0.06, 0.09, 0.02, 0.0, 0.0, 0.01, 0.0, 0.0]]
    )


def test_text_above_table_and_encoded_headers():
    template_test('test_data/test5.xls',
        [[u"Indicateur de nationalit\xe9 condens\xe9 (Fran\xe7ais/\xc9tranger) Etrangers, Sexe Femmes, \xc2ge regroup\xe9 (8 classes d'\xe2ge) 25 \xe0 39 ans, Lieu de r\xe9sidence 1 an auparavant Autre r\xe9gion en France m\xe9tropolitaine", u"Indicateur de nationalit\xe9 condens\xe9 (Fran\xe7ais/\xc9tranger) Etrangers, Sexe Femmes, \xc2ge regroup\xe9 (8 classes d'\xe2ge) 25 \xe0 39 ans, Lieu de r\xe9sidence 1 an auparavant Autre r\xe9gion dans un Dom", u"Indicateur de nationalit\xe9 condens\xe9 (Fran\xe7ais/\xc9tranger) Etrangers, Sexe Femmes, \xc2ge regroup\xe9 (8 classes d'\xe2ge) 25 \xe0 39 ans, Lieu de r\xe9sidence 1 an auparavant Collectivit\xe9 d'outre-mer (Com)"]],
        [[u'Miquelon-Langlade', u'Saint-Pierre', u'Saint-Barth\xe9lemy', u'Saint-Martin'], [u'97501', u'97502', u'97701', u'97801']],
        [[0.0, 0.0, 0.0], [0.995145943187555, 0.0, 0.0], [3.04936075597554, 0.0, 0.0], [8.338262356680618, 9.7769083799724, 0.0]]
    )


def test_empty_data_row():
    template_test('test_data/test6.xls',
        [[u"Chiffre d'affaires total", u"Chiffre d'affaires \xe0 l'exportation", u"Chiffre d'affaires net France", u'Production stock\xe9e']],
        [[u'TOTAL', u'Activit\xe9s de poste et de courrier', u'T\xe9l\xe9communications']],
        [[3181980.0, 506766.0, 2675214.0, 7121.0], [None, None, None, None], [57301.0, 2819.0, 54481.0, 4.0]]
    )


def test_notations_of_missing_values():
    template_test('test_data/test6b.xls',
        [[u'Ensemble', u'Fran\xe7ais', u"Etranger de l'Union Europ\xe9enne", u'Etranger hors Union Europ\xe9enne']],
        [[u'Ensemble', u'Pas de dipl\xf4me ou CEP, BEPC, Brevet \xe9l\xe9mentaire, Brevet des coll\xe8ges', u'CAP, BEP', u'Baccalaur\xe9at technologique, professionnel ou g\xe9n\xe9ral', u'Dipl\xf4me technique de premier cycle (BTS, DEUST, DUT...) ou dipl\xf4me universitaire ou g\xe9n\xe9ral de premier cycle, dipl\xf4mes des professions sociales ou de la sant\xe9, infirmier(\xe8res)', u"Dipl\xf4me universitaire de deuxi\xe8me cycle ou de troisi\xe8me cycle ou dipl\xf4me d'ing\xe9nieur, d'une grande \xe9cole"]],
        [[100.0, None, 2.0, 4.5], [100.0, 87.0, 3.1, None], [100.0, 95.7, None, None], [None, 94.7, 2.2, 3.1], [100.0, 96.5, 10.8, None], [100.0, None, None, None]]
    )


def test_notations_of_missing_values_2():
    template_test('test_data/test6c.xls',
        [[u'Pop. active occup\xe9e non salari\xe9e (en milliers)', u'Dur\xe9e moyenne hebdomadaire non salari\xe9s (en heures)']],
        [[u'Industries extractives', u'Fabrication de denr\xe9es alimentaires, de boissons et de produits \xe0 base de tabac']],
        [[0.0, None], [None, 58.1]]
    )


def test_notations_of_missing_values_3():
    template_test('test_data/test6d.xls',
        [[u'a 1978', u'a 1979']],
        [[u"Fabrication d'\xe9quipements \xe9lectriques, \xe9lectroniques, informatiques ; fabrication de machines", u"Activit\xe9s des m\xe9nages en tant qu'employeurs", u'Total des branches'], [u'A17.C3', u'A38.TZ', u'TOTAL']],
        [[31.9, 32.9], [None, None], [3144.4, 3261.0]]
    )


def test_notations_of_missing_values_4():
    template_test_header_rows('test_data/test6e.xls',
        [[u'AZ', u'Agriculture, sylviculture et p\xeache'], [u'=DE+C1+C2+C3+C4+C5', u'Industries manufacturi\xe8res, industries extractives et autres'], [u'DE', u'Industries extractives, \xe9nergie, eau, gestion des d\xe9chets et s\xe9pollution'], [u'C1', u'Fabrication de denr\xe9es alimentaires, de boissons et de produits \xe0 base de tabac'], [u'C2', u'Cok\xe9faction et raffinage'], [u'C3', u"Fabrication d'\xe9quipements \xe9lectriques, informatiques ; fabrication de machines"], [u'C4', u'Fabrication de mat\xe9riels de transport'], [u'C5', u"Fabrication d'autres produits industriels"], [u'FZ', u'Construction'], [u'=GZ+HZ+IZ', u'Commerce de gros et de d\xe9tail, transports, h\xe9bergement et restauration'], [u'GZ', u"Commerce ; r\xe9paration d'automobiles et de motocycles"], [u'HZ', u'Transport et entreposage'], [u'IZ', u'H\xe9bergement et restauration'], [u'JZ', u'Information et communication'], [u'KZ', u"Activit\xe9s financi\xe8res et d'assurance"], [u'LZ', u'Activit\xe9s immobili\xe8res'], [u'MN', u'Activit\xe9s scientifiques et techniques ; services administratifs et de soutien'], [u'OQ', u'Administration publique, enseignement, sant\xe9 humaine et action sociale'], [u'RU', u'Autres activit\xe9s de services'], [u'Total', '']],
        [[u'Alsace-Champagne-Ardenne-Lorraine'], [u'Aquitaine-Limousin-Poitou-Charentes'], [u'Auvergne-Rh\xf4ne-Alpes'], [u'Bourgogne-Franche-Comt\xe9'], [u'Bretagne'], [u'Centre-Val de Loire'], [u'Corse'], [u'\xcele-de-France'], [u'Languedoc-Roussillon-Midi-Pyr\xe9n\xe9es'], [u'Nord-Pas-de-Calais-Picardie'], [u'Normandie'], [u'Pays de la Loire'], [u"Provence-Alpes-C\xf4te d'Azur"], [u'Province'], [u'France m\xe9tropolitaine'], [u'Guadeloupe'], [u'Martinique'], [u'Guyane'], [u'R\xe9union'], [u'Mayotte'], [u'Dom'], [u'Hors territoire'], [u'France enti\xe8re']]
    )



def test_no_data_cells():
    template_test('test_data/test_no_data_cells.xls',
        [],
        [],
        []
    )


def test_extract_all_sheets():
    import os
    for excel_file in os.listdir('test_data/'):
        if '.xls' in excel_file:
            extractor = ExcelExtractor()
            extractor.extract_file('test_data/%s' % excel_file)


def test_header_in_many_rows1():
    template_test_header_rows('test_data/test_header_in_many_rows.xls',
        [[u'Total', u'N\xe9s vivants'], [u'Gar\xe7ons', u'N\xe9s vivants'], [u'Filles', u'N\xe9s vivants'], [u'Total', u'Enfants sans vie'], [u'Gar\xe7ons', u'Enfants sans vie'], [u'Filles', u'Enfants sans vie'], [u'Gar\xe7ons vivants pour 100 n\xe9s vivants'], [u'Gar\xe7ons vivants pour 100 filles vivantes'], [u'Gar\xe7ons sans vie pour 100 filles sans vie']],
        [[u'1901'], [u'2012']]
    )


def test_header_in_many_rows2():
    template_test_header_rows('test_data/test_header_in_many_rows2.xls',
        [[u'Ensemble'], [u'moins de 20 ans', u"\xc2ge atteint dans l'ann\xe9e du p\xe8re"], [u'20 \xe0 24 ans', u"\xc2ge atteint dans l'ann\xe9e du p\xe8re"], [u'25 \xe0 29 ans', u"\xc2ge atteint dans l'ann\xe9e du p\xe8re"], [u'30 \xe0 34 ans', u"\xc2ge atteint dans l'ann\xe9e du p\xe8re"], [u'35 \xe0 39 ans', u"\xc2ge atteint dans l'ann\xe9e du p\xe8re"], [u'40 \xe0 44 ans', u"\xc2ge atteint dans l'ann\xe9e du p\xe8re"], [u'45 \xe0 49 ans', u"\xc2ge atteint dans l'ann\xe9e du p\xe8re"], [u'50 \xe0 54 ans', u"\xc2ge atteint dans l'ann\xe9e du p\xe8re"], [u'55 \xe0 59 ans', u"\xc2ge atteint dans l'ann\xe9e du p\xe8re"], [u'60 ans ou plus', u"\xc2ge atteint dans l'ann\xe9e du p\xe8re"]],
        [[u'moins de 20 ans', u'1993 ou apr\xe8s'], [u'20 \xe0 24 ans', u'1992 - 1988'], [u'25 \xe0 29 ans', u'1987 - 1983'], [u'30 \xe0 34 ans', u'1982 - 1978'], [u'35 \xe0 39 ans', u'1977 - 1973'], [u'40 \xe0 44 ans', u'1972 - 1968'], [u'45 \xe0 49 ans', u'1967 - 1963'], [u'50 \xe0 60 ans', u'1962 - 1952'], [u'Ensemble', u'Ensemble']]
    )


def test_header_in_many_rows4():
    template_test_header_rows('test_data/test_header_in_many_rows4.xls',
        [[u"Nombre total d'accou- chements"], [u'2.0 gar\xe7ons vivants', u'Accouchements de jumeaux ayant donn\xe9'], [u'1 gar\xe7on vivant 1 gar\xe7on sans vie', u'Accouchements de jumeaux ayant donn\xe9'], [u'2.0 gar\xe7ons sans vie', u'Accouchements de jumeaux ayant donn\xe9'], [u'1 gar\xe7on vivant 1 fille vivante', u'Accouchements de jumeaux ayant donn\xe9'], [u'1 gar\xe7on vivant 1 fille sans vie', u'Accouchements de jumeaux ayant donn\xe9'], [u'1 gar\xe7on sans vie 1 fille vivante', u'Accouchements de jumeaux ayant donn\xe9'], [u'1 gar\xe7on sans vie 1 fille sans vie', u'Accouchements de jumeaux ayant donn\xe9'], [u'2 filles vivantes', u'Accouchements de jumeaux ayant donn\xe9'], [u'1 fille vivante 1 fille sans vie', u'Accouchements de jumeaux ayant donn\xe9'], [u'2 filles sans vie', u'Accouchements de jumeaux ayant donn\xe9']],
        [[u'13 \xe0 14 ans', u'1994 - 1993'], [u'15 \xe0 19 ans', u'1992 - 1988'], [u'20 \xe0 24 ans', u'1987 - 1983'], [u'25 \xe0 29 ans', u'1982 - 1978'], [u'30 \xe0 34 ans', u'1977 - 1973'], [u'35 \xe0 39 ans', u'1972 - 1968'], [u'40 \xe0 44 ans', u'1967 - 1963'], [u'45 \xe0 49 ans', u'1962 - 1958'], [u'50 \xe0 54 ans', u'1957 - 1953'], ['Total']]
    )


def test_header_in_many_rows4b():
    template_test_header_rows('test_data/test_header_in_many_rows4b.xls',
        [[u"Nombre total d'accou- chements"], [u'2.0 gar\xe7ons vivants', u'Accouchements de jumeaux ayant donn\xe9'], [u'1 gar\xe7on vivant 1 gar\xe7on sans vie', u'Accouchements de jumeaux ayant donn\xe9'], [u'2.0 gar\xe7ons sans vie', u'Accouchements de jumeaux ayant donn\xe9'], [u'1 gar\xe7on vivant 1 fille vivante', u'Accouchements de jumeaux ayant donn\xe9'], [u'1 gar\xe7on vivant 1 fille sans vie', u'Accouchements de jumeaux ayant donn\xe9'], [u'1 gar\xe7on sans vie 1 fille vivante', u'Accouchements de jumeaux ayant donn\xe9'], [u'1 gar\xe7on sans vie 1 fille sans vie', u'Accouchements de jumeaux ayant donn\xe9'], [u'2 filles vivantes', u'Accouchements de jumeaux ayant donn\xe9'], [u'1 fille vivante 1 fille sans vie', u'Accouchements de jumeaux ayant donn\xe9'], [u'2 filles sans vie', u'Accouchements de jumeaux ayant donn\xe9']],
        [[u'13 \xe0 14 ans', u'1994 - 1993'], [u'15 \xe0 19 ans', u'1992 - 1988'], [u'20 \xe0 24 ans', u'1987 - 1983'], [u'25 \xe0 29 ans', u'1982 - 1978'], [u'30 \xe0 34 ans', u'1977 - 1973'], [u'35 \xe0 39 ans', u'1972 - 1968'], [u'40 \xe0 44 ans', u'1967 - 1963'], [u'45 \xe0 49 ans', u'1962 - 1958'], [u'50 \xe0 54 ans', u'1957 - 1953'], ['Total']]
    )


def test_header_in_many_rows7():
    template_test_header_rows('test_data/test_header_in_many_rows7.xls',
        [[u'Population moyenne'], [u'Mariages (a)'], [u'Divorces (b)'], [u'N\xe9s vivants (c)'], [u'D\xe9c\xe9d\xe9s (d)'], [u'Accrois- sement naturel'], [u'Accrois- sement total'], [u'Nuptialit\xe9', u'Taux pour 1 000 habitants'], [u'Natalit\xe9', u'Taux pour 1 000 habitants'], [u'Mortalit\xe9', u'Taux pour 1 000 habitants'], [u'Accrois- sement naturel', u'Taux pour 1 000 habitants'], [u'Accrois- sement total', u'Taux pour 1 000 habitants']],
        [[u'Paris'], [u'Seine-et-Marne'], [u'Autres'], [u'France m\xe9tropolitaine (f)'], [u'France (g)']]
    )


def test_aggregated_header_row_but_not_merged():
    template_test_header_rows('test_data/test_aggregated_header_row_but_not_merged.xls',
        [[u'AZ'], [u'DE', u'Energie, eau, d\xe9chets'], [u'C', u'Total', u'Branches manufacturi\xe8res'], [u'C1', u'Industrie agro-alimentaires', u'Branches manufacturi\xe8res'], [u'C2', u'Cok\xe9faction et raffinage', u'Branches manufacturi\xe8res'], [u'C3', u"Biens d'\xe9quipement", u'Branches manufacturi\xe8res'], [u'C4', u'Mat\xe9riels de transport', u'Branches manufacturi\xe8res'], [u'C5', u'Autres branches industriels', u'Branches manufacturi\xe8res'], [u'(DE) \xe0 (C5)', u'Branches industrielles'], [u'FZ', u'Construction'], [u'GZ', u'Commerce'], [u'HZ', u'Transport'], [u'IZ', u'H\xe9bergement-restauration'], [u'JZ', u'Information-communication'], [u'KZ', u'Services financiers'], [u'LZ', u'Services immobiliers'], [u'MN', u'Services aux entreprises'], [u'OQ', u'Services non marchands'], [u'RU', u'Services aux m\xe9nages'], [u'(GZ) \xe0 (MN), (RU)', u'Services marchands'], [u'(DE) \xe0 (MN), (RU)', u'Branches marchandes non agricoles'], [u'TOTAL']],
        [[u'1949T1'], [u'1949T2'], [u'1949T3'], [u'1949T4'], [u'1950T1'], [u'1950T2'], [u'1950T3'], [u'1950T4'], [u'1951T1'], [u'1951T2'], [u'1951T3'], [u'1951T4'], [u'1952T1'], [u'1952T2'], [u'1952T3'], [u'1952T4'], [u'1953T1'], [u'1953T2'], [u'1953T3'], [u'1953T4'], [u'1954T1'], [u'1954T2'], [u'1954T3'], [u'1954T4'], [u'1955T1'], [u'1955T2'], [u'1955T3'], [u'1955T4'], [u'1956T1'], [u'1956T2'], [u'1956T3'], [u'1956T4'], [u'1957T1'], [u'1957T2'], [u'1957T3'], [u'1957T4'], [u'1958T1'], [u'1958T2'], [u'1958T3'], [u'1958T4'], [u'1959T1'], [u'1959T2'], [u'1959T3'], [u'1959T4'], [u'1960T1'], [u'1960T2'], [u'1960T3'], [u'1960T4'], [u'1961T1'], [u'1961T2'], [u'1961T3'], [u'1961T4'], [u'1962T1'], [u'1962T2'], [u'1962T3'], [u'1962T4'], [u'1963T1'], [u'1963T2'], [u'1963T3'], [u'1963T4'], [u'1964T1'], [u'1964T2'], [u'1964T3'], [u'1964T4'], [u'1965T1'], [u'1965T2'], [u'1965T3'], [u'1965T4'], [u'1966T1'], [u'1966T2'], [u'1966T3'], [u'1966T4'], [u'1967T1'], [u'1967T2'], [u'1967T3'], [u'1967T4'], [u'1968T1'], [u'1968T2'], [u'1968T3'], [u'1968T4'], [u'1969T1'], [u'1969T2'], [u'1969T3'], [u'1969T4'], [u'1970T1'], [u'1970T2'], [u'1970T3'], [u'1970T4'], [u'1971T1'], [u'1971T2'], [u'1971T3'], [u'1971T4'], [u'1972T1'], [u'1972T2'], [u'1972T3'], [u'1972T4'], [u'1973T1'], [u'1973T2'], [u'1973T3'], [u'1973T4'], [u'1974T1'], [u'1974T2'], [u'1974T3'], [u'1974T4'], [u'1975T1'], [u'1975T2'], [u'1975T3'], [u'1975T4'], [u'1976T1'], [u'1976T2'], [u'1976T3'], [u'1976T4'], [u'1977T1'], [u'1977T2'], [u'1977T3'], [u'1977T4'], [u'1978T1'], [u'1978T2'], [u'1978T3'], [u'1978T4'], [u'1979T1'], [u'1979T2'], [u'1979T3'], [u'1979T4'], [u'1980T1'], [u'1980T2'], [u'1980T3'], [u'1980T4'], [u'1981T1'], [u'1981T2'], [u'1981T3'], [u'1981T4'], [u'1982T1'], [u'1982T2'], [u'1982T3'], [u'1982T4'], [u'1983T1'], [u'1983T2'], [u'1983T3'], [u'1983T4'], [u'1984T1'], [u'1984T2'], [u'1984T3'], [u'1984T4'], [u'1985T1'], [u'1985T2'], [u'1985T3'], [u'1985T4'], [u'1986T1'], [u'1986T2'], [u'1986T3'], [u'1986T4'], [u'1987T1'], [u'1987T2'], [u'1987T3'], [u'1987T4'], [u'1988T1'], [u'1988T2'], [u'1988T3'], [u'1988T4'], [u'1989T1'], [u'1989T2'], [u'1989T3'], [u'1989T4'], [u'1990T1'], [u'1990T2'], [u'1990T3'], [u'1990T4'], [u'1991T1'], [u'1991T2'], [u'1991T3'], [u'1991T4'], [u'1992T1'], [u'1992T2'], [u'1992T3'], [u'1992T4'], [u'1993T1'], [u'1993T2'], [u'1993T3'], [u'1993T4'], [u'1994T1'], [u'1994T2'], [u'1994T3'], [u'1994T4'], [u'1995T1'], [u'1995T2'], [u'1995T3'], [u'1995T4'], [u'1996T1'], [u'1996T2'], [u'1996T3'], [u'1996T4'], [u'1997T1'], [u'1997T2'], [u'1997T3'], [u'1997T4'], [u'1998T1'], [u'1998T2'], [u'1998T3'], [u'1998T4'], [u'1999T1'], [u'1999T2'], [u'1999T3'], [u'1999T4'], [u'2000T1'], [u'2000T2'], [u'2000T3'], [u'2000T4'], [u'2001T1'], [u'2001T2'], [u'2001T3'], [u'2001T4'], [u'2002T1'], [u'2002T2'], [u'2002T3'], [u'2002T4'], [u'2003T1'], [u'2003T2'], [u'2003T3'], [u'2003T4'], [u'2004T1'], [u'2004T2'], [u'2004T3'], [u'2004T4'], [u'2005T1'], [u'2005T2'], [u'2005T3'], [u'2005T4'], [u'2006T1'], [u'2006T2'], [u'2006T3'], [u'2006T4'], [u'2007T1'], [u'2007T2'], [u'2007T3'], [u'2007T4'], [u'2008T1'], [u'2008T2'], [u'2008T3'], [u'2008T4'], [u'2009T1'], [u'2009T2'], [u'2009T3'], [u'2009T4'], [u'2010T1'], [u'2010T2'], [u'2010T3'], [u'2010T4'], [u'2016T1'], [u'2016T2']]
    )


def test_many_empty_cells_in_data_rows():
    template_test_header_rows('test_data/test_many_empty_cells_in_data_rows.xls',
        [[u'Production'], [u'Importations'], [u'Imp\xf4ts sur imports'], [u'Marges de commerce'], [u'Autres imp\xf4ts sur produits'], [u'Subventions sur produits'], [u'TVA non d\xe9ductible'], [u'Total Ressources'], [u'Consom- mation interm\xe9diaires'], [u'CF des Menages Autoconsomm\xe9e'], [u'CF des Menages Commercialis\xe9e'], [u'CF des Administrations publiques'], [u'CF des ISBLM'], [u'F.B.C.F.'], [u'Variation des stocks'], [u'Exportations'], [u'Total Emploi final'], [u'Total emploi']],
        [[u'Total', u'Total'], [u'AGRICULTURE, CHASSE, SERVICES ANNEXES', u'002'], [u'SYLVICULTURE, EXPLOITATION FORESTIERE', u'003'], [u'PECHE, AQUACULTURE', u'004'], [u'SUCRE, RHUM', u'005'], [u'VIANDES ET LAIT', u'006'], [u'AUTRES IAA', u'007'], [u'INDUSTRIE DES BIENS DE CONSOMMATION', u'008'], [u"INDUSTRIE DES BIENS D'EQUIPEMENT", u'009'], [u'INDUSTRIE DES PRODUITS MINERAUX', u'010'], [u'AUTRES INDUSTRIES DE BIENS INTERMEDIAIRES', u'011'], [u'PRODUCTION DE COMBUSTIBLES ET CARBURANTS', u'012'], [u'EAU ET ELECTRICITE', u'013'], [u'CONSTRUCTION', u'014'], [u'COMMERCE ET REPARATION AUTOMOBILE', u'015'], [u'COMMERCE', u'016'], [u'TRANSPORTS, POSTES ET TELECOMMUNICATIONS', u'017 & 020'], [u'ACTIVITES FINANCIERES', u'018'], [u'ACTIVITES IMMOBILIERES', u'019'], [u'AUTRES SERVICES AUX ENTREPRISES', u'021'], [u'HOTELS ET RESTAURANTS', u'022'], [u'AUTRES SERVICES AUX PARTICULIERS', u'023'], [u'EDUCATION, SANTE, ACTION SOCIALE', u'024'], [u'ADMINISTRATIONS', u'025'], [u'CORRECTION TERRITORIALE', u'CTR']]
    )


def test_many_empty_cells_in_data_rows2():
    template_test_header_rows('test_data/test_many_empty_cells_in_data_rows2.xls',
        [['1989'], ['1990'], ['1991'], ['1992'], ['1993'], ['1994'], ['1995'], ['1996'], ['1997'], ['1998'], ['1999'], ['2000'], ['2001'], ['2002'], ['2003'], ['2004']],
        [[u"Chiffre d'affaires hors TVA"], [u'Ventes de marchandises'], [u"- Co\xfbt d'achat des marchandises"], [u'= Marge commerciale (au prix de base)'], [u'+ Production vendue de biens et services'], [u'+ Production stock\xe9e et immobilis\xe9e'], [u'= Production totale au prix de base'], [u'- Consommations interm\xe9diaires'], [u'= Valeur ajout\xe9e au prix de base'], [u'Valeur ajout\xe9e'], [u"+ Subventions d'exploitation"], [u'- Frais de personnel'], [u'- Imp\xf4ts et taxes sur la production'], [u"= Exc\xe9dent brut d'exploitation"], [u"Exc\xe9dent brut d'exploitation"], [u"+ Autres produits d'exploitation et financier"], [u"- Autres charges d'exploitation et financi\xe8res"], [u'= Profit brut courant avant imp\xf4t']]
    )


def test_many_empty_cells_in_data_rows3():
    template_test_header_rows('test_data/test_many_empty_cells_in_data_rows3.xls',
        [[u"Nombre d'entreprises"], [u'Effectifs salari\xe9s au 31/12/2014'], [u"Effectifs salari\xe9s d\xe9di\xe9s \xe0 l'activit\xe9 a\xe9ronautique et spatiale"]],
        [[u'Aquitaine'], [u'0-9 salari\xe9s', u'Aquitaine'], [u'10-249 salari\xe9s', u'Aquitaine'], [u'250 salari\xe9s ou plus', u'Aquitaine'], [u'INDUSTRIE', u'Aquitaine'], [u'0-9 salari\xe9s', u'INDUSTRIE', u'Aquitaine'], [u'10-249 salari\xe9s', u'INDUSTRIE', u'Aquitaine'], [u'250 salari\xe9s ou plus', u'INDUSTRIE', u'Aquitaine'], [u'TERTIAIRE', u'Aquitaine'], [u'0-9 salari\xe9s', u'TERTIAIRE', u'Aquitaine'], [u'10-249 salari\xe9s', u'TERTIAIRE', u'Aquitaine'], [u'250 salari\xe9s ou plus', u'TERTIAIRE', u'Aquitaine'], [u'Midi-Pyr\xe9n\xe9es'], [u'0-9 salari\xe9s', u'Midi-Pyr\xe9n\xe9es'], [u'10-249 salari\xe9s', u'Midi-Pyr\xe9n\xe9es'], [u'250 salari\xe9s ou plus', u'Midi-Pyr\xe9n\xe9es'], [u'INDUSTRIE', u'Midi-Pyr\xe9n\xe9es'], [u'0-9 salari\xe9s', u'INDUSTRIE', u'Midi-Pyr\xe9n\xe9es'], [u'10-249 salari\xe9s', u'INDUSTRIE', u'Midi-Pyr\xe9n\xe9es'], [u'250 salari\xe9s ou plus', u'INDUSTRIE', u'Midi-Pyr\xe9n\xe9es'], [u'TERTIAIRE', u'Midi-Pyr\xe9n\xe9es'], [u'0-9 salari\xe9s', u'TERTIAIRE', u'Midi-Pyr\xe9n\xe9es'], [u'10-249 salari\xe9s', u'TERTIAIRE', u'Midi-Pyr\xe9n\xe9es'], [u'250 salari\xe9s ou plus', u'TERTIAIRE', u'Midi-Pyr\xe9n\xe9es'], [u'Grand Sud-Ouest'], [u'0-9 salari\xe9s', u'Grand Sud-Ouest'], [u'10-249 salari\xe9s', u'Grand Sud-Ouest'], [u'250 salari\xe9s ou plus', u'Grand Sud-Ouest'], [u'INDUSTRIE', u'Grand Sud-Ouest'], [u'0-9 salari\xe9s', u'INDUSTRIE', u'Grand Sud-Ouest'], [u'10-249 salari\xe9s', u'INDUSTRIE', u'Grand Sud-Ouest'], [u'250 salari\xe9s ou plus', u'INDUSTRIE', u'Grand Sud-Ouest'], [u'TERTIAIRE', u'Grand Sud-Ouest'], [u'0-9 salari\xe9s', u'TERTIAIRE', u'Grand Sud-Ouest'], [u'10-249 salari\xe9s', u'TERTIAIRE', u'Grand Sud-Ouest'], [u'250 salari\xe9s ou plus', u'TERTIAIRE', u'Grand Sud-Ouest']]
    )



def test_empty_columns_in_the_right_of_table():
    template_test_header_rows('test_data/test_empty_columns_in_the_right_of_table.xls',
        [['2013.0'], ['2014 sd'], ['2015 p']],
        [[u'Ensemble des services marchands (100 %)'], [u'Services principalement orient\xe9s vers les entreprises (35 %)'], [u'Activit\xe9s sp\xe9cialis\xe9es, scientifiques et techniques marchandes (19 %)'], [u'Activit\xe9s de services administratifs et de soutien (16 %)'], [u'Information et communication (15 %)'], [u'Services principalement orient\xe9s vers les m\xe9nages (50 %)'], [u'Activit\xe9s immobili\xe8res (38 %)'], [u'H\xe9bergement et restauration (8 %)'], [u'Arts, spectacles et activit\xe9s r\xe9cr\xe9atives marchands (1 %)'], [u'Autres activit\xe9s de services marchandes (3 %)'], [u'Pour m\xe9moire : produit int\xe9rieur brut']]
    )


def test_empty_columns_in_the_right_of_table2():
    template_test_header_rows('test_data/test_empty_columns_in_the_right_of_table2.xls',
        [[u'Effectif', u'R\xe9sidence principale', u'Cat\xe9gorie de logement'], [u'%', u'R\xe9sidence principale', u'Cat\xe9gorie de logement'], [u'Effectif', u'R\xe9sidence secondaire', u'Cat\xe9gorie de logement'], [u'%', u'R\xe9sidence secondaire', u'Cat\xe9gorie de logement'], [u'Effectif', u'Logement occasionnel', u'Cat\xe9gorie de logement'], [u'%', u'Logement occasionnel', u'Cat\xe9gorie de logement'], [u'Effectif', u'Logement vacant', u'Cat\xe9gorie de logement'], [u'%', u'Logement vacant', u'Cat\xe9gorie de logement'], [u'Effectif', u'Ensemble', u'Cat\xe9gorie de logement'], [u'%', u'Ensemble', u'Cat\xe9gorie de logement']],
        [[u'601001 - Acoua'], [u'601002 - Mtsangadoua'], [u'602001 - Bandraboua'], [u'602002 - Handr\xe9ma'], [u'602003 - Mtsangamboua'], [u'602004 - Dzoumogn\xe9'], [u'Ensemble']]
    )
