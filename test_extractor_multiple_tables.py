from base_test import extract_multiple_tables

def test_multiple_tables1():
    extract_multiple_tables('test_data/test_multiple_tables_per_sheet.xls',
        [
            (
                [[u'Pays'], [u'Bretagne']],
                [[u'1er d\xe9cile', u'1er d\xe9cile'], [u'M\xe9diane', u'M\xe9diane'], [u'9e d\xe9cile', u'9e d\xe9cile'], [u'Rapport inter d\xe9cile (9e d\xe9cile/1er d\xe9cile)', u'Rapport inter d\xe9cile (9e d\xe9cile/1er d\xe9cile)'], [u'Part des salaires (en %)', u'Part des salaires (en %)'], [u'Part des retraites (en %)', u'Part des retraites (en %)'], [u'Part des b\xe9n\xe9fices (en %)', u'Part des b\xe9n\xe9fices (en %)'], [u'Part des autres revenus (en %)', u'Part des autres revenus (en %)'], [u'Part des m\xe9nages impos\xe9s (en %)', u'Part des m\xe9nages impos\xe9s (en %)']]
            ),
            (
                [[u'Pays'], [u'Bretagne'], [u'Poids Pays / Bretagne (en %)']],
                [[u'Nombre d\u2019allocataires :'], [u'dont percevant le RSA socle'], [u'dont percevant l\u2019AAH'], [u'dont percevant une APL']]
            )
        ]
    )

def test_multiple_tables2():
    extract_multiple_tables('test_data/test_multiple_tables_per_sheet2.xls',
        [
            (
                [['1996.0'], ['1997.0'], ['1998.0'], ['2010.0'], ['2011.0']],
                [[u'PIB1'], [u'Consommation finale dont :'], [u'Consommation finale des m\xe9nages'], [u'Consommation finale des administrations publiques'], [u'FBCF2'], [u'Variations de stocks'], [u'Solde ext\xe9rieur yc correction territoriale3'], [u'- Exportations'], [u'- Importations yc co\xfbt assurance fr\xeat']]
            ),
            (
                [['1996.0'], ['1997.0'], ['1998.0'], ['2010.0'], ['2011.0']],
                [[u'PIB'], [u'Consommation finale dont :'], [u'Consommation finale des m\xe9nages'], [u'Consommation finale des administrations publiques'], [u'FBCF'], [u'Solde ext\xe9rieur yc correction territoriale3'], [u'- Exportations'], [u'- Importations yc co\xfbt assurance fr\xeat']]
            )
        ]
    )

def test_multiple_tables2b():
    extract_multiple_tables('test_data/test_multiple_tables_per_sheet2b.xls',
        [
            (
                [['1996.0'], ['1997.0'], ['1998.0'], ['2010.0'], ['2011.0']],
                [[u'PIB1'], [u'Consommation finale dont :'], [u'Consommation finale des m\xe9nages'], [u'Consommation finale des administrations publiques'], [u'FBCF2'], [u'Variations de stocks'], [u'Solde ext\xe9rieur yc correction territoriale3'], [u'- Exportations'], [u'- Importations yc co\xfbt assurance fr\xeat']]
            ),
            (
                [['1996.0'], ['1997.0'], ['1998.0'], ['2010.0'], ['2011.0']],
                [[u'PIB'], [u'Consommation finale dont :'], [u'Consommation finale des m\xe9nages'], [u'Consommation finale des administrations publiques'], [u'FBCF'], [u'Solde ext\xe9rieur yc correction territoriale3'], [u'- Exportations'], [u'- Importations yc co\xfbt assurance fr\xeat']]
            ),
            (
                [['1996.0'], ['1997.0'], ['1998.0'], ['2010.0'], ['2011.0']],
                [[u'Consommation finale dont :'], [u'Consommation finale des m\xe9nages'], [u'Consommation finale des administrations publiques'], [u'FBCF'], [u'Variations de stocks'], [u'Solde ext\xe9rieur yc correction territoriale3']]
            )
        ]
    )

def test_multiple_tables3():
    extract_multiple_tables('test_data/test_multiple_tables_per_sheet3.xls',
        [
            (
                [['1999.0', u'Pays'], ['2008.0', u'Pays'], ['2013.0', u'Pays'], [u'Poids Pays / Bretagne en 2013 (en %)']],
                [[u'Population']]
            ),
            (
                [[u'2008/2013']],
                [[u'Pays'], [u'Bretagne']]
            ),
            (
                [[u'Pays'], [u'Bretagne']],
                [[u'Solde naturel'], [u'Solde migratoire'], [u'Taux de variation annuel (en %)'], [u'd\xfb au solde naturel (en %)'], [u'd\xfb au solde apparent des entr\xe9es et des sorties (en %)']]
            ),
            (
                [['2013.0', u'Pays'], ['2014.0', u'Pays'], ['2013.0', u'Bretagne'], ['2014.0', u'Bretagne']],
                [[u'Naissances'], [u'D\xe9c\xe8s']]
            ),
            (
                [['2007.0', u'Pays'], ['2012.0', u'Pays'], [u'Poids Pays / Bretagne en 2012 (en %)']],
                [[u'Moins de 15 ans'], [u'15 \xe0 24 ans'], [u'25 \xe0 54 ans'], [u'55 \xe0 64 ans'], [u'65 ans ou plus'], [u'Ensemble']]
            ),
            (
                [[u'Pays'], [u'Poids Pays / Bretagne (en %)']],
                [[u'Agriculteurs exploitants'], [u"Artisans, commer\xe7ants et chefs d'entreprise"], [u'Cadres et professions intellectuelles sup\xe9rieures'], [u'Professions interm\xe9diaires'], [u'Employ\xe9s'], [u'Ouvriers'], [u'Retrait\xe9s'], [u'Autres personnes sans activit\xe9 professionnelle'], [u'Ensemble']]
            )
        ]
    )

def test_multiple_tables4():
    expected_headers = [
        (
            [[u'Pays'], [u'Bretagne']],
            [[u'Population totale'], [u'Population active'], [u'Part de la population active (en %)'], [u'Actifs ayant un emploi'], [u'Part des actifs ayant un emploi (en %)'], [u'Ch\xf4meurs'], [u'Taux de ch\xf4mage* (en %)']]
        ),
        (
            [[u'3e trim', u'2014.0', u'Pays'], [u'4e trim', u'2014.0', u'Pays'], [u'1er trim', u'2015.0', u'Pays'], [u'2e trim', u'2015.0', u'Pays'], [u'3e trim', u'2015.0', u'Pays'], [u'Pays R\xe9partition au 30/09/15 (en %)'], [u'R\xe9partition au 30/09/15 (en %)', u'Bretagne'], [u'Poids Pays / Bretagne au 30/09/15 (en %)']],
            [[u'Hommes'], [u'Femmes'], [u'Total'], [u'Moins de 25 ans'], [u'50 ans ou plus']]
        )
    ]
    extract_multiple_tables('test_data/test_multiple_tables_per_sheet4.xls', expected_headers)
    extract_multiple_tables('test_data/test_multiple_tables_per_sheet4b.xls', expected_headers)

def test_multiple_tables5():
    extract_multiple_tables('test_data/test_multiple_tables_per_sheet5.xls',
        [
            (
                [[u'Nombre', u'Pays'], [u'%', u'Pays'], [u'%', u'Bretagne'], [u'Poids Pays / Bretagne (en %)']],
                [[u'0 salari\xe9 ou effectif inconnu'], [u'1 \xe0 9 salari\xe9s'], [u'10 \xe0 19 salari\xe9s'], [u'20 \xe0 99 salari\xe9s'], [u'100 \xe0 499 salari\xe9s'], [u'500 salari\xe9s ou plus'], [u'Total']]
            ),
            (
                [[u'Pays', u"Nombre d'\xe9tablissements actifs"], [u'Poids Pays / Bretagne (en %)', u"Nombre d'\xe9tablissements actifs"], [u'Pays', u'Postes salari\xe9s'], [u'Poids Pays / Bretagne (en %)', u'Postes salari\xe9s']],
                [[u'Agriculture'], [u'Industrie'], [u'Construction'], [u'Commerce'], [u'Services'], [u'Total']]
            ),
            (
                [[u'Pays'], [u'Poids Pays / Bretagne (en %)']],
                [[u'Total'], [u'dont autoentrepreneurs']]
            )
        ]
    )

def test_multiple_tables6():
    expected_headers = [
        (
            [['2013.0', u'Pays'], ['2014.0', u'Pays'], ['2015.0', u'Pays'], ['2013.0', u'Bretagne'], ['2014.0', u'Bretagne'], ['2015.0', u'Bretagne'], [u'Poids Pays / Bretagne en 2015 (en %)']],
            [[u'Logements autoris\xe9s'], [u'Individuels'], [u'Collectifs']]
        ),
        (
            [[u'Pays'], [u'Poids Pays / Bretagne (en %)']],
            [[u'R\xe9sidence principale'], [u'Logement occasionnel'], [u'R\xe9sidence secondaire'], [u'Logement vacant'], [u'Ensemble']]
        ),
        (
            [[u'Nombre'], [u'% par rapport au nombre total de logements']],
            [[u'Pays'], [u'Bretagne']]
        )
    ]
    extract_multiple_tables('test_data/test_multiple_tables_per_sheet6.xls', expected_headers)
    extract_multiple_tables('test_data/test_multiple_tables_per_sheet6b.xls', expected_headers)
