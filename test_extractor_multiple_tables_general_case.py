from base_test import extract_multiple_tables

def test_multiple_tables0():
    extract_multiple_tables('test_data/test_multiple_tables0b.xls',
        [
            (
                [[u'Production des produits (1)'], [u'Importations de biens'], [u'Importations de services'], [u'TOTAL DES RESSOURCES (3)']],
                [[u'AZ'], [u'DE'], [u'PCAFAB'], [u'TOTAL']]
            ),
            (
                [[u'AZ']],
                [[u'Consommation interm\xe9diaire', u'P2'], [u'Valeur ajout\xe9e brute', u'B1g'], [u'PRODUCTION DES BRANCHES', u'P1'], [u'Production marchande', u'P11'], [u'Prod. pour emploi final propre', u'P12'], [u'Production non marchande', u'P13']]
            ),
            (
                [[u'AZ'], [u'TOTAL']],
                [[u'Agriculture, sylviculture et p\xeache', u'AZ'], [u'Industries extractives, \xe9nergie, eau, gestion des d\xe9chets et d\xe9pollution', u'DE'], [u'Correction CAF/FAB', u'PCAFAB'], [u'TOTAL', u'TOTAL']]
            )
        ]
    )

def test_multiple_tables2():
    extract_multiple_tables('test_data/test_multiple_tables2b.xls',
        [
            (
                [[u'AZ'], [u'TOTAL']],
                [[u'Consommation interm\xe9diaire', u'P2'], [u'Valeur ajout\xe9e brute', u'B1g'], [u'PRODUCTION DES BRANCHES', u'P1'], [u'Production marchande', u'P11'], [u'Prod. pour emploi final propre', u'P12'], [u'Production non marchande', u'P13']]
            ),
            (
                [[u'Production des produits (1)'], [u'Importations de biens'], [u'Importations de services'], [u'TOTAL DES RESSOURCES (3)']],
                [[u'AZ'], [u'DE'], [u'PCAFAB'], [u'TOTAL']]
            ),
            (
                [[u'AZ'], [u'TOTAL']],
                [[u'Agriculture, sylviculture et p\xeache', u'AZ'], [u'Industries extractives, \xe9nergie, eau, gestion des d\xe9chets et d\xe9pollution', u'DE'], [u'Correction CAF/FAB', u'PCAFAB'], [u'TOTAL', u'TOTAL']]
            )
        ]
    )

epectation_test_a = [
    (
        [['x1'], ['x2']],
        [['y1'], ['y2']]
    ),
    (
        [['x1'], ['x2'], ['x3'], ['x4']],
        [['y1'], ['y2'], ['y3']]
    ),
    (
        [['x1'], ['x2']],
        [['y1'], ['y2']]
    )
]
def test_a():
    extract_multiple_tables('test_data/multiple-tables-general-case/a.xls', epectation_test_a)

def test_a2():
    extract_multiple_tables('test_data/multiple-tables-general-case/a2.xls', epectation_test_a)

def test_a3():
    extract_multiple_tables('test_data/multiple-tables-general-case/a3.xls', epectation_test_a)

# def test_a4():
# 	extract_multiple_tables('test_data/multiple-tables-general-case/a4.xls', epectation_test_a)
