import os
import shutil

import unzip

TEST_FOLDER = 'test_data/unzip_test'


def template_test(zip_file, expected_extracted_files, delete_extracted_folder=True):
    path = os.path.join(TEST_FOLDER, zip_file)
    files = unzip.extract_zip_file(path)
    print files
    assert sorted(files) == sorted(expected_extracted_files)

    if delete_extracted_folder:
        shutil.rmtree(path.replace('.zip', ''), ignore_errors=True)


def test_unzip_single_file():
    template_test(
        'single_file.zip',
        [os.path.join(TEST_FOLDER, 'single_file', '1.xls')]
    )


def test_unzip_folder():
    template_test(
        'folder.zip',
        [os.path.join(TEST_FOLDER, 'folder', '2.xls'), os.path.join(TEST_FOLDER, 'folder', '3.xls')]
    )


def test_unzip_nested_folder():
    template_test(
        'nested_folder.zip',
        [
            os.path.join(TEST_FOLDER, 'nested_folder', '1.xls'),
            os.path.join(TEST_FOLDER, 'nested_folder', '2.xls'),
            os.path.join(TEST_FOLDER, 'nested_folder', 'nested', '3.xls')
        ]
    )


def test_unzip_nested_folder_with_zip():
    template_test(
        'nested_folder_with_zip.zip',
        [
            os.path.join(TEST_FOLDER, 'nested_folder_with_zip', '4.xls'),
            os.path.join(TEST_FOLDER, 'nested_folder_with_zip', 'nested', '3.xls'),
            os.path.join(TEST_FOLDER, 'nested_folder_with_zip', 'one_two', '1.xls'),
            os.path.join(TEST_FOLDER, 'nested_folder_with_zip', 'one_two', '2.xls')
        ]
    )
