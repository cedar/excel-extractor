# -*- coding: UTF-8 -*-


import collections

import xlrd

import variable_decoder as decoder
from variable_decoder import build_list_varibles


def build_simple_list_variables():
    list_variables = collections.OrderedDict()

    list_variables['X'] = dict()
    list_variables['X']['1'] = 'A'
    list_variables['X']['2'] = 'B'

    list_variables['y'] = dict()
    list_variables['y']['10'] = 'c'
    list_variables['y']['11'] = 'd'

    return list_variables


def build_complex_list_variables():
    list_variables = collections.OrderedDict()

    list_variables['X'] = dict()
    list_variables['X']['1'] = 'A'
    list_variables['X']['2'] = 'B'

    list_variables['z_w'] = dict()
    list_variables['z_w']['7'] = 'omega'

    return list_variables


def test1():
    assert decoder.decode('X1_y10', build_simple_list_variables()) == 'A, c'
    assert decoder.decode('X2_y11', build_simple_list_variables()) == 'B, d'


def test2():
    assert decoder.decode('X2_z_w7', build_complex_list_variables()) == 'B, omega'


def test4():
    assert decoder.decode('y11_X2', build_simple_list_variables()) == 'd, B'
