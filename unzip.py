import os
import zipfile

import xlrd

from file_utils import list_directory

"""
Utility functions for unzipping files
"""

def extract_zip_file(path):
    file_name = os.path.basename(path).replace('.zip', '')
    extracted_folder = os.path.join(os.path.dirname(path), file_name)
    if not os.path.exists(extracted_folder):
        os.makedirs(extracted_folder)

    all_files = list()
    with zipfile.ZipFile(path) as zip_file:
        zip_file.extractall(extracted_folder)

        for extracted_file in list_directory(extracted_folder):
            f = os.path.join(extracted_folder, extracted_file)

            if '.zip' in extracted_file:
                all_files.extend(extract_zip_file(f))
            elif os.path.isdir(f):
                all_files.extend(filter_and_extract(f))
            elif '.xls' in extracted_file:
                all_files.append(f)
    return all_files

def filter_and_extract(folder):
    all_files = list()
    for f in list_directory(folder):
        path = os.path.join(folder, f)
        if '.zip' in f:
            all_files.extend(extract_zip_file(path))
        elif '.xls' in f:
            all_files.append(path)
        elif os.path.isdir(path):
            all_files.extend(filter_and_extract(path))

    return all_files
