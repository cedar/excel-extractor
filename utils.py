import re
import hashlib
from itertools import groupby
from operator import itemgetter

"""
Miscellaneous utility functions
"""


def clean_empty_spaces(text):
    return ' '.join(text.split()).replace('\t', ' ').replace('\n', ' ')


def group_nearby_values(list_numbers, max_distance):
    groups = list()
    groups.append(list())

    current_value = list_numbers[0]
    for number in list_numbers:
        if number - current_value > max_distance:
            groups.append(list())
        current_value = number
        groups[-1].append(number)

    return groups


def match_exact(pattern, text):
    match = re.match(pattern, text)
    if match:
        return match.group(0) == text
    return False


def str_list(objs):
    return ', '.join(map(str, objs))


def force_encode(value):
    if type(value) == str:
        # Ignore errors even if the string is not proper UTF-8 or has
        # broken marker bytes.
        # Python built-in function unicode() can do this.
        value = unicode(value, "utf-8", errors="ignore")
    else:
        # Assume the value object has proper __unicode__() method
        value = unicode(value)
    return value


def hash_code(string):
    return hashlib.sha224(string).hexdigest()


def split_extractor_input_line(line):
    ''' Return `downloaded_date,url,file,description,published_date` from csv
        input file of the extractor

    Args:
      line (str): a line from csv input file
    '''
    tokens = line.split(',')
    downloaded_date, url, file_name = tokens[0:3]
    description = ' '.join(tokens[3:-1])
    published_date = tokens[-1]
    return downloaded_date, url, file_name, description, published_date
