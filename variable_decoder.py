import collections

import xlrd

from data_model import VariableSheet
from excel_utils import ExcelValidator

"""
Utility functions to decode the encoded headers from Exccel files.
These functions were specific designed for INSEE's excel files.
"""


def decode(encoded_header, list_variables, code_name_definions=None):
    if '*' in encoded_header:
        encoded_header = encoded_header[:encoded_header.index('*')]

    decoded = ''

    varibles = list_variables.keys()
    dict_variable_indexes = dict()
    for variable in varibles:
        if variable in encoded_header:
            dict_variable_indexes[variable] = encoded_header.index(variable)

    sub_header = encoded_header
    for sorted_variable in sorted(dict_variable_indexes, key=dict_variable_indexes.get):
        mapping = list_variables[sorted_variable]
        sub_header = sub_header[len(sorted_variable):]

        if '_' in sub_header:
            idx = sub_header.index('_')
            key = sub_header[:idx]
            sub_header = sub_header[idx + 1:]
        else:
            key = sub_header

        value = mapping[key] if key in mapping else ''

        if code_name_definions is not None:
            decoded += code_name_definions[sorted_variable] + ' '
        decoded += value + ', '

    return decoded.rstrip()[:-1]

def build_list_varibles(sheet):
    def is_variable_cell(cell):
        return cell.ctype == xlrd.XL_CELL_TEXT and ':' in cell.value

    def get_variable_and_definition(cell):
        text = cell.value
        definition = text[text.index(':'):].replace(':', '').strip()
        variable = text[:text.index(':')].replace(':', '').strip()
        return variable, definition

    num_of_rows = sheet.nrows
    num_of_columns = sheet.ncols

    list_variables = collections.OrderedDict()
    code_name = None
    code_name_definions = dict()

    for rx in range(num_of_rows):
        row = sheet.row(rx)
        first_cell = row[0]
        if is_variable_cell(first_cell):
            variable, definition = get_variable_and_definition(first_cell)
            if definition == '':
                continue

            process_new_variable = False
            if rx - 1 >= 0:
                above_cell = sheet.row(rx - 1)[0]
                if is_variable_cell(above_cell):
                    above_var, above_def = get_variable_and_definition(above_cell)
                    if above_def == '':
                        process_new_variable = True

                    # above variable is number and current variable is in upper case
                    try:
                        int(above_var)
                        if variable.isupper():
                            process_new_variable = True
                    except ValueError:
                        pass
                else:
                    is_upper_cell_empty = ExcelValidator.is_cell_empty_or_blank(above_cell)
                    if is_upper_cell_empty:
                        process_new_variable = True

            if process_new_variable:
                code_name = variable
                code_name_definions[code_name] = definition
                list_variables[code_name] = dict()
                continue

            if code_name is not None:
                list_variables[code_name][variable] = definition

    # print list_variables
    # print code_name_definions
    return VariableSheet(list_variables, code_name_definions)
