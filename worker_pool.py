import multiprocessing
import time

from log_utils import logger


class Worker(multiprocessing.Process):
    """
    A process that could be terminated after a specific timeout
    """

    def __init__(self, target, args, name, timeout):
        super(Worker, self).__init__(target=target, args=args, name=name)
        self.timeout = timeout
        self.start_time = time.time()

    def has_exceeded_timeout(self):
        return time.time() - self.start_time > self.timeout


def parallel(function, arguments, step, timeout):
    """
    Multiprocessing with timeout option to terminate long running process

    Args:
        function: name of the function that should be run in parallel
        arguments: each item of list `arguments` is one valid argument for `function` to execute,
        step (`float`): the amount of time (in seconds) that a worker pool (`worker_pool.py`) should wait before
            verify statuses of its processes
        timeout (`int`): the maximum amount of time (in seconds) for each process to finish its task
    """
    # leave 2 cores for system
    max_processes = multiprocessing.cpu_count() - 2
    list_processes = list()

    # check if any process should be terminated
    def has_some_workers():
        for process in list_processes:
            timeout = process.has_exceeded_timeout()
            if not process.is_alive() or timeout:
                if timeout:
                    logger.exception('[TimeoutError] ' + process.name)
                process.terminate()
                process.join()
                list_processes.remove(process)
        return len(list_processes) == 0

    # check list of processes if we can add new process
    def add_workers():
        if len(list_processes) < max_processes and len(arguments) > 0:
            for i in range(max_processes - len(list_processes)):
                if len(arguments) > 0:
                    arg = arguments[0]
                    arguments.remove(arguments[0])
                    p = Worker(target=function, args=(arg,), timeout=timeout, name=str(arg))
                    list_processes.append(p)
                    p.start()

    add_workers()
    while not has_some_workers():
        add_workers()
        time.sleep(step)
